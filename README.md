# pro-manager
The pro-manager application provides user interfaces
to account-tender and pro-tender services
in order to manage production and professional services.

## License
This application is unlicensed public domain open source.
See the UNLICENSE file unlicense.org for more information.

## Installation and configuration
Get the module, install required modules, then start with npm.

## Quick use
Start and test with npm start and npm test.

## General use
After starting with npm start use a browser to access the active port.

## Documentation
The docs folder has documents explaining intended usage.
