// account.js
// handle account intereactions: request, sign in/out, inspect, save, reset, delete

// internal modules
let config = require('./config');
let local = require('./localize');
let service = require('./service');

// external modules
let pino = require('pino')();
let clientSource = require('node-rest-client').Client;

// get account request form
// !!! REFACTOR IN PROGRESS REPLACES THIS WITH ACCOUNT PANEL
function accountRequest(language, res, ref) {
    pino.info('get account request, ref = %s', ref);
        // language is passed from req.session.language which is not accessable here
        // show languages, attributes, and input only creds
        let requestSectionText = local.text(language, 'account-request');
        // attribute section
        // labels for section and attribute elements
        let attributesLabel = local.text(language, 'attributes-label');
        let typeLabel = local.text(language, 'type');
        let newTypeLabel = local.text(language, 'new-type');
        let valueLabel = local.text(language, 'value-label');
        let newValueLabel = local.text(language, 'new-value-label');
        let qualifierLabel = local.text(language, 'qualifier-label');
        let newQualifierLabel = local.text(language, 'new-qualifier-label');
        let deleteText = local.text(language, 'delete');
        // add attribute option shows empty fields with add button
        let addText = local.text(language, 'add');
        let newAttributeText = local.text(language, 'new-attribute');
        let challengeText = local.text(language, 'challenge-label');
        let responseText = local.text(language, 'response-label');
        // button action text: request, close, explain
        let requestAccountText = local.text(language, 'account-request');
        let closeText = local.text(language, 'close');
        let explainText = local.text(language, 'account-explain');
    if (ref === undefined) {
        // request base uncredentialed account to edit

        // !!! INDENTING
        // note attributes should be in group by type order
    let baseAttributes = [
        'contact', '', '',
        'language', language, 'primary',
        'name', '', ''
    ];
    var requestArgs = {
        data: {
            attributes: baseAttributes,
            credentials: []
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('requesting uncredentialed account with seed data: %s', JSON.stringify(requestArgs));
    service.post(config.accountTender + '/account/request', requestArgs, function(requestResult) {
        if (requestResult.result === 'failed') {
            // failed case should protest and gracefully stop--not clear how to do this
            return;
        }
        // !!! INDENTINGE
        
        // loop for all existing attributes showing data and a delete button
        // attribute index is array position, type, value, qualifier are text values,
        // typeId, valueId, qualId are tags for interface widgets
        var attrsText = [];
        for (var attrIndex = 0; attrIndex < baseAttributes.length / 3; attrIndex++) {
            pino.info('attribute #%d: type=%s, value=%s, qual=%s', attrIndex,
                baseAttributes[attrIndex * 3],
                baseAttributes[attrIndex * 3 + 1],
                baseAttributes[attrIndex * 3 + 2]);
            attrsText[attrIndex] = {
                index: attrIndex,
                type: baseAttributes[attrIndex * 3],
                typeOut: 'type-out-' + attrIndex,
                typeIn: 'type-in-' + attrIndex,
                value: baseAttributes[attrIndex * 3 + 1],
                valueOut: 'value-out-' + attrIndex,
                valueIn: 'value-in-' + attrIndex,
                qualifier: baseAttributes[attrIndex * 3 + 2],
                qualOut: 'qual-out-' + attrIndex,
                qualIn: 'qual-in-' + attrIndex,
                delId: 'del' + attrIndex
            };
        }
        // loop for all existing attributes showing data and a delete button
        // note: attributes should start with empty name and contact records
        //let refText = local.text(language, requestResult.ref);
        let refText = requestResult.ref;
        // render account request panel using all the generated text
        res.render('account-request', {
            ref: refText,
            request_section_label: requestSectionText,
            attributes_label: attributesLabel,
            type_label: typeLabel,
            new_type_label: newTypeLabel,
            value_label: valueLabel,
            new_value_label: newValueLabel,
            qualifier_label: qualifierLabel,
            new_qualifier_label: newQualifierLabel,
            delete_text: deleteText,
            attrs: attrsText,
            new_attribute: newAttributeText,
            add_text: addText,
            request_account: requestAccountText,
            close: closeText,
            explain_label: explainText
        });  // end response render block
    });
    return;  // superfluous?
    }
    else {
        // !!! compose request args using the ref that is not undefined
        // args: account given, but permit and persona not
    var inspectArgs = {
        data: {
            account: ref
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('ACCOUNT INSPECTION ARGS: %s', JSON.stringify(inspectArgs));
            service.get(config.accountTender + '/account/inspect', inspectArgs, function(inspectResult) {
pino.info('ACCOUNT INSPECTION RESULT: %s', JSON.stringify(inspectResult));
        // loop for all existing attributes showing data and a delete button
        // attribute index is array position, type, value, qualifier are text values,
        // typeId, valueId, qualId are tags for interface widgets
        var attrsText = [];
        for (var attrIndex = 0; attrIndex < inspectResult.attributes.length / 3; attrIndex++) {
            pino.info('attribute #%d: type=%s, value=%s, qual=%s', attrIndex,
                inspectResult.attributes[attrIndex * 3],
                inspectResult.attributes[attrIndex * 3 + 1],
                inspectResult.attributes[attrIndex * 3 + 2]);
            attrsText[attrIndex] = {
                index: attrIndex,
                type: inspectResult.attributes[attrIndex * 3],
                typeOut: 'type-out-' + attrIndex,
                typeIn: 'type-in-' + attrIndex,
                value: inspectResult.attributes[attrIndex * 3 + 1],
                valueOut: 'value-out-' + attrIndex,
                valueIn: 'value-in-' + attrIndex,
                qualifier: inspectResult.attributes[attrIndex * 3 + 2],
                qualOut: 'qual-out-' + attrIndex,
                qualIn: 'qual-in-' + attrIndex,
                delId: 'del' + attrIndex
            };
        }
        // loop for all existing attributes showing data and a delete button
        // note: attributes should start with empty name and contact records
        //let refText = local.text(language, requestResult.ref);
        //let refText = ref;  // not requestResult.ref ?!
        // render account request panel using all the generated text
        res.render('account-request', {
            ref: ref,
            request_section_label: requestSectionText,
            attributes_label: attributesLabel,
            type_label: typeLabel,
            new_type_label: newTypeLabel,
            value_label: valueLabel,
            new_value_label: newValueLabel,
            qualifier_label: qualifierLabel,
            new_qualifier_label: newQualifierLabel,
            delete_text: deleteText,
            attrs: attrsText,
            new_attribute: newAttributeText,
            add_text: addText,
            request_account: requestAccountText,
            close: closeText,
            explain_label: explainText
        });  // end response render block
    });
        
    }
}

// accountCredential raises a panel for input of credentials
// currently this is a challenge, response, and confirmation of the response
// in future needs support to enable return code from email authentication step
function accountCredential(language, ref, res) {
    pino.info('request account credential: ref = %s', JSON.stringify(ref));
    let credentialSectionText = local.text(language, 'credential-section');
    let challengeText = local.text(language, 'challenge-label');
    let responseText = local.text(language, 'response-label');
    let confirmResponseText = local.text(language, 'confirm-response-label');
    let requestCompleteText = local.text(language, 'request-complete');
    let requestAnotherText = local.text(language, 'request-another');
    let closeText = local.text(language, 'close');
    res.render('credential', {
        ref: ref,
        credential_section_label: credentialSectionText,
        challenge_label: challengeText,
        response_label: responseText,
        confirm_response_label: confirmResponseText,
        request_complete: requestCompleteText,
        request_another: requestAnotherText,
        close: closeText
    });
}

// accountComplete saves and account with a set of credentials
// and leaves the account in a credentialed, completed state
function accountComplete(accountRef, challenge, response, responseConfirm, callback) {
    pino.info('account complete: account = %s, challenge = %s, response = %s, confirm = %s', accountRef, challenge, response, responseConfirm);
    // note misalignment of interfaces:
    // account tender has explicit difference between credential and completion steps
    // while here for convenience of user interaction
    // steps are either adding a credential or adding last credential and completing the process
    let credentialArgs = {
        data: {
            account: accountRef,
            challenge: challenge,
            response: response,
            confirm: responseConfirm
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for credential request: %s', JSON.stringify(credentialArgs));
    service.post(config.accountTender + '/account/credential', credentialArgs, function(credentialResult) {
        pino.info('credential request result: %s', JSON.stringify(credentialResult));
        // if credential result is failure then end here?
        // now complete the account request by marking it as credentialed
        let completionArgs = {
            data: {},
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('arguments for credential request: %s', JSON.stringify(completionArgs));
        service.post(config.accountTender + '/account/complete/' + accountRef, completionArgs, function(completionResult) {
            pino.info('account completion result: %s', JSON.stringify(completionResult));
            callback(completionResult);
        });
    });  // end credential post block
}

// !!! REFACTOR IN PROGRESS AND ALSO IN CONFUSION
// !!! this is in conflict with the input panel display routine
// accountAddCredential adds credential to account
// but strictly speaking does not complete the credentialing process
// ... account.addCredential(message.account, message.challenge, message.response, message.confirm, function(result) 
function accountAddCredential(accountRef, challenge, response, responseConfirm, callback) {
    pino.info('account credential: account = %s, challenge = %s, response = %s, confirm = %s', accountRef, challenge, response, responseConfirm);
    let credentialArgs = {
        data: {
            account: accountRef,
            challenge: challenge,
            response: response,
            confirm: responseConfirm
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for credential request: %s', JSON.stringify(credentialArgs));
    service.post(config.accountTender + '/account/credential', credentialArgs, function(credentialResult) {
        pino.info('credential request result: %s', JSON.stringify(credentialResult));
        callback(credentialResult);
    });
}

// get reference from name (changed completely with id refactor)
// intended to primarily use the name, but may disambiguate with contact
// which could be email or phone or whatever else (chat?)
// !!! should also allow pronoun for disambiguation?
function accountReferences(nameString, contactString, addressString, pronounString, callback) {
    pino.info('getting account references name = %s, contact = %s, address = %s, pronoun = %s', nameString, contactString, addressString, pronounString);
    var referencesArgs = {
        data: {
            name: nameString,
            contact: contactString,
            address: addressString,
            pronoun: pronounString
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for references request: %s', JSON.stringify(referencesArgs));
    service.get(config.accountTender + '/account/references', referencesArgs, function(data) {
        pino.info('references fetch result: %s', JSON.stringify(data));
        callback(data);
    });
}

function accountSign(language, res) {
    pino.info('account sign');
    let signSectionText = local.text(language, 'sign-section');
    let accountNameLabel = local.text(language, 'account-name-label');
    let accountContactLabel = local.text(language, 'contact-label');
    let accountAddressLabel = local.text(language, 'address-label');
    let accountPronounLabel = local.text(language, 'pronoun-label');
    let signInText = local.text(language, 'account-sign-in');
    let closeText = local.text(language, 'close');
    res.render('sign', {
        sign_section_label: signSectionText,
        account_name_label: accountNameLabel,
        account_contact_label: accountContactLabel,
        account_address_label: accountAddressLabel,
        account_pronoun_label: accountPronounLabel,
        sign_account: signInText,
        close: closeText,
    });
}

// fetch authentication form
// account is an id reference
function accountChallenge(language, account, permit, res) {
    pino.info('get account challenge: language = %s, account = %s, permit = %s', language, account, permit);
    // asking for a challenge indicates there is no permit yet ???
    let challengeArgs = {
        data: {
            permit: permit,
            account: account
        },
        headers: {'Content-Type': 'application/json'}
    };
    service.get(config.accountTender + '/account/challenge', challengeArgs, function (data) {
        if (data === undefined) {
            pino.info('no challenge returned');
            return;
        }
        pino.info('challenge returned: %s', data.challenge);
        let challengeText = local.text(language, 'challenge-label');
        let responseText = local.text(language, 'response-label');
        let submitText = local.text(language, 'response-submit');
        let closeText = local.text(language, 'close');
        res.render('challenge', {
            account: account,
            challenge_label: challengeText,
            challenge_value: data.challenge,
            response_label: responseText,
            submit_response: submitText,
            close_text: closeText
        });  // end render challenge response block
    });
}

function accountRequestChallenge(accountRef, callback) {
    pino.info('account request challenge: account = %s', accountRef);
    // query account service 
    // !!! refactor to work with account tender changes
    var requestChallengeArgs = {
        data: {
            account: accountRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    service.get(config.accountTender + '/account/challenge',
        requestChallengeArgs, function (data) {  
        pino.info('challenge fetch result: %s, challenge: %s',
            data.result, data.challenge);
        callback(data);
    });
}

function accountRespond(account, response, callback) {
    pino.info('account respond');
    let postResponseArgs = {
        data: {
            account: account,
            response: response
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('post response args: %s', JSON.stringify(postResponseArgs));
    service.post(config.accountTender + '/account/response', postResponseArgs, function(data) {
        pino.info('response result: %s', JSON.stringify(data));
        callback(data);
    });
}

// accountInspect loads and renders a panel with account data ready for editing
function accountInspect(language, accountRef, permit, res) {
    pino.info('get account inspection from account %s', accountRef);
    // get account data
    var inspectargs = {
        data: {
            permit: permit,
            account: accountRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    service.get(config.accountTender + '/account/inspect', inspectargs, function (accountData) {
        pino.info('inspection result: %s', JSON.stringify(accountData));
        if (accountData === undefined || accountData.result === 'failed') {
            pino.info('account inspection failed');
            return;
        }
        // get translated strings
        let inspectText = local.text(language, 'account-section-label');
        // display language is translated from language code which is not shown
        // !!! language is now an attribute, so display is code for now
        //let languageLabelText = local.text(language, 'language-label');
        //let languageValueText = local.text(language, accountData.language);
        // relevant timestamps excluding credentialed
        let createdLabelText = local.text(language, 'created-label');
        let viewedLabelText = local.text(language, 'viewed-label');
        let modifiedLabelText = local.text(language, 'modified-label');
        // roles and status
        let rolesLabelText = local.text(language, 'roles-label');
        let statusLabelText = local.text(language, 'status-label');
        // attribute section: type, value, qualifier and delete, then an add new option
        let attributesLabel = local.text(language, 'attributes-label');
        let typeLabel = local.text(language, 'type');
        let valueLabel = local.text(language, 'value-label');
        let qualifierLabel = local.text(language, 'qualifier-label');
        let deleteText = local.text(language, 'delete');
        let newTypeLabel = local.text(language, 'new-type');
        let newValueLabel = local.text(language, 'new-value-label');
        let newQualifierLabel = local.text(language, 'new-qualifier-label');
        let newAttributeText = local.text(language, 'new-attribute');
        // loop for all existing attributes to show labels, data, and a delete button
        // attribute index is array position, type, value, qualifier are text values,
        // typeId, valueId, qualId are tags for interface widgets
        var attrsText = [];
        for (var attrIndex = 0; attrIndex < accountData.attributes.length / 3; attrIndex++) {
            pino.info('attribute strings #%d: type=%s, value=%s, qual=%s', attrIndex,
                accountData.attributes[attrIndex * 3],
                accountData.attributes[attrIndex * 3 + 1],
                accountData.attributes[attrIndex * 3 + 2]);
            attrsText[attrIndex] = {
                index: attrIndex,
                type: accountData.attributes[attrIndex * 3],
                typeOut: 'type-out-' + attrIndex,
                typeIn: 'type-in-' + attrIndex,
                value: accountData.attributes[attrIndex * 3 + 1],
                valueOut: 'value-out-' + attrIndex,
                valueIn: 'value-in-' + attrIndex,
                qualifier: accountData.attributes[attrIndex * 3 + 2],
                qualOut: 'qual-out-' + attrIndex,
                qualIn: 'qual-in-' + attrIndex,
                delId: 'del' + attrIndex
            };
        }  // end attribute loop block
        // action buttons at bottom of form: close, explain 
        let closeText = local.text(language, 'close');
        let explainText = local.text(language, 'account-explain');
        // now render the panel using all fetched and translated text
        res.render('account-inspect', {
            ref: accountRef,
            inspect_section: inspectText,
            //language_label: languageLabelText,
            //language_value: languageValueText,
            created_label: createdLabelText,
            created_value: accountData.created,
            viewed_label: viewedLabelText,
            viewed_value: accountData.viewed,
            modified_label: modifiedLabelText,
            modified_value: accountData.modified,
            roles_label: rolesLabelText,
            roles_value: accountData.roles,
            status_label: statusLabelText,
            status_value: accountData.status,
            attributes_label: attributesLabel,
            type_label: typeLabel,
            value_label: valueLabel,
            qualifier_label: qualifierLabel,
            delete_text: deleteText,
            attrs: attrsText,
            new_type_label: newTypeLabel,
            new_value_label: newValueLabel,
            new_qualifier_label: newQualifierLabel,
            new_attribute: newAttributeText,
            close: closeText,
            explain_label: explainText
        });  // end response render block
    })  // end account inspection block
}

// persona related routines stashed here during refactor
function accountPersonaStuff() {
        // personas: first sketch lists them
        // idea is to have buttons that raise persona inspector
        // persona list args: {"data":{"account":"6abbb323-793b-4256-bdfb-25e399da1252","permit":"71e93599-d646-4ba9-8167-cdc7c03f498c"},"headers":{"Content-Type":"application/json"}}
        //personas list fetched: {"result":"okay","reason":"personas-read","personas":["DEFAULT_PERSONA"]}
        //persona records args: {"data":{"account":"6abbb323-793b-4256-bdfb-25e399da1252","permit":"71e93599-d646-4ba9-8167-cdc7c03f498c"},"headers":{"Content-Type":"application/json"}}
        let personasListArgs = {
            data: {
                permit: permit,
                account: account
            },
            headers: {'Content-Type': 'application/json'}
        };
        console.log('persona list args: %s', JSON.stringify(personasListArgs));
        // !!! STOPPED WITH SKETCH HERE
        // notes and action whole form action buttons
        //let notesText = local.text(language, 'notes-label');
        //let notesValueText = data.notes;
        let closeText = local.text(language, 'close');
        //let updateText = local.text(language, 'account-save');
        //let deleteText = local.text(language, 'delete');
        // raise account inspect/update panel with accumulated data
}

// accountPanel is a generalization used for request, inspect, and reset
// it is invoked from accountRequestPanel, accountInspect, and accountReset
// mode -- request, inspect, or reset
// account -- refers to account being inspected or reset, empty if request
// permit -- for account permission to inspect
// language -- display language used for labels and controls on panels
// seedAttrs is an array of type, value, qualifier triples to seed the account data
// res is the response structure for completing response
// callback gets called with the result
//
// timestamps for inspect
// !!! refactor in progress
// basics: request gets language only with given default
//     inspect gets all with input only for language
// attributes: inspect shows data, inspect and request get inputs
// personas: inspect shows data, request gets inputs
// cred: request and reset get input only
//
// !!! currently this is not active and accountRequest and accountInspect are used
//     and accountReset has never yet been made to work 
// !!! should probably not have a callback
function accountPanel(mode, account, permit, language, accountRef, res) {
    // first sketch shares essentially zero work
    // but uses the same inputs and callback options
    // !!! may have to change to accept previous drafts as substrate?
    if (mode === 'request') {
        // set up seed account with lang default and empty name and contact
        // first sketch used cookie storage and had odd failures
        // now an uncredentialed account gets set up for this
    
        service.post(config.accountTender + '/account/request', {attributes: seedAttrs}, function(requestResult) {
            
            // !!! INDENTING
        // show languages, attributes, and input only creds
        let requestSectionText = local.text(language, 'account-request');
        // attribute section
        // labels for section and attribute elements
        let attributesLabel = local.text(language, 'attributes-label');
        let typeLabel = local.text(language, 'type');
        let newTypeLabel = local.text(language, 'new-type');
        let valueLabel = local.text(language, 'value-label');
        let newValueLabel = local.text(language, 'new-value-label');
        let qualifierLabel = local.text(language, 'qualifier-label');
        let newQualifierLabel = local.text(language, 'new-qualifier-label');
        // loop for all existing attributes showing data and a delete button
        // attribute index is array position
        // type, value, qualifier are text values
        // typeId, valueId, qualId are tags for interface widgets
        // ... might be most correct to read attributes from account data
        var attrsText = [];
        for (var attrIndex = 0; attrIndex < seedAttrs.length / 3; attrIndex++) {
            pino.info('attribute #%d: type=%s, value=%s, qual=%s', attrIndex,
                seedAttrs[attrIndex * 3],
                seedAttrs[attrIndex * 3 + 1],
                seedAttrs[attrIndex * 3 + 2]);
            attrsText[attrIndex] = {
                index: attrIndex,
                type: seedAttrs[attrIndex * 3],
                typeOut: 'type-out-' + attrIndex,
                typeIn: 'type-in-' + attrIndex,
                value: seedAttrs[attrIndex * 3 + 1],
                valueOut: 'value-out-' + attrIndex,
                valueIn: 'value-in-' + attrIndex,
                qualifier: seedAttrs[attrIndex * 3 + 2],
                qualOut: 'qual-out-' + attrIndex,
                qualIn: 'qual-in-' + attrIndex,
                delId: 'del' + attrIndex
            };
        }
        // loop for all existing attributes showing data and a delete button
        // note: attributes should start with empty name and contact records
        let refText = local.text(language, requestResult.ref);
        let deleteText = local.text(language, 'delete');
        // add attribute option shows empty fields with add button
        let addText = local.text(language, 'add');
        let newAttributeText = local.text(language, 'new-attribute');
        let challengeText = local.text(language, 'challenge-label');
        let responseText = local.text(language, 'response-label');
        // button action text: request, close, explain
        let requestAccountText = local.text(language, 'account-request');
        let closeText = local.text(language, 'close');
        let explainText = local.text(language, 'account-explain');
        // render account request panel using all the generated text
        res.render('account-request', {
            ref: refText,
            request_section_label: requestSectionText,
            attributes_label: attributesLabel,
            type_label: typeLabel,
            new_type_label: newTypeLabel,
            value_label: valueLabel,
            new_value_label: newValueLabel,
            qualifier_label: qualifierLabel,
            new_qualifier_label: newQualifierLabel,
            delete_text: deleteText,
            attrs: attrsText,
            new_attribute: newAttributeText,
            add_text: addText,
            request_account: requestAccountText,
            close: closeText,
            explain_label: explainText
        });  // end response render block
        //callback();  // should just be return and ignored by caller?
            // !!! END OF INDENTING ISSUES
        });  // !!! end request uncredentialed account for composition
    }
    else if (mode === 'inspect') {
        
        // starting with code from request -- boilerplate substrate only
        // show languages, attributes, and input only creds
        let inspectSectionText = local.text(language, 'account-inspect');
        // attribute section
        // labels for section and attribute elements
        let attributesLabel = local.text(language, 'attributes-label');
        let typeLabel = local.text(language, 'type');
        let newTypeLabel = local.text(language, 'new-type');
        let valueLabel = local.text(language, 'value-label');
        let newValueLabel = local.text(language, 'new-value-label');
        let qualifierLabel = local.text(language, 'qualifier-label');
        let newQualifierLabel = local.text(language, 'new-qualifier-label');
        // loop for all existing attributes showing data and a delete button
        // attribute index is array position
        // type, value, qualifier are text values
        // typeId, valueId, qualId are tags for interface widgets
        // !!! instead of seedAttrs inspect must pull from account records
        
        let inspectArgs = {
            data: {
                permit: permit,
                account: account
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.get(config.accountTender + '/account/inspect', inspectArgs, function (accountData) {
            // !!! INDENTINGE
        var attrsText = [];
        for (var attrIndex = 0; attrIndex < accountData.attributes.length / 3; attrIndex++) {
            pino.info('attribute #%d: type=%s, value=%s, qual=%s', attrIndex,
                accountData.attributes[attrIndex * 3],
                accountData.attributes[attrIndex * 3 + 1],
                accountData.attributes[attrIndex * 3 + 2]);
            attrsText[attrIndex] = {
                index: attrIndex,
                type: accountData.attributes[attrIndex * 3],
                typeOut: 'type-out-' + attrIndex,
                typeIn: 'type-in-' + attrIndex,
                value: accountData.attributes[attrIndex * 3 + 1],
                valueOut: 'value-out-' + attrIndex,
                valueIn: 'value-in-' + attrIndex,
                qualifier: accountData.attributes[attrIndex * 3 + 2],
                qualOut: 'qual-out-' + attrIndex,
                qualIn: 'qual-in-' + attrIndex,
                delId: 'del' + attrIndex
            };
        }
        pino.info('attributes text array construction: %s', JSON.stringify(attrsText));

        let deleteText = local.text(language, 'delete');
        // add attribute option shows empty fields with add button
        let addText = local.text(language, 'add');
        let newAttributeText = local.text(language, 'new-attribute');
        // button action text: request, close, explain
        let updateText = local.text(language, 'update');
        let closeText = local.text(language, 'close');
        let explainText = local.text(language, 'account-explain');
        // render account request panel using all the generated text
        res.render('account-inspect', {
            inspect_section_label: inspectSectionText,
            attributes_label: attributesLabel,
            type_label: typeLabel,
            new_type_label: newTypeLabel,
            value_label: valueLabel,
            new_value_label: newValueLabel,
            qualifier_label: qualifierLabel,
            new_qualifier_label: newQualifierLabel,
            delete_text: deleteText,
            attrs: attrsText,
            new_attribute: newAttributeText,
            add_text: addText,
            update: updateText,
            close: closeText,
            explain_label: explainText
        }); 
        //callback();  // should just be return and ignored by caller?
        
    });  // end basic account inspection
        
    }
    else if (mode === 'reset') {
        // credentials input only
    }
    else {
        // this should never happen, so error or bad invocation to report
        //callback({result: 'failed', reason: 'invalid-mode'});
    }
}

// account.summary(accountRef, permitRef, eventRecord.client, function(clientValue)
// get account summary from account tender service
// note that results are returned directly instead of in the usual result and reason bundle
function accountSummary(accountRef, permitRef, targetRef, callback) {
    
    let inspectArgs = {
        data: {
            account: accountRef,
            permit: permitRef,
            ref: targetRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    service.get(config.accountTender + '/account/summary', inspectArgs, function (accountData) {
        pino.info('account summary: %s', accountData);
        callback(accountData);
    });
}

// stringInList is a utility function
// that takes a string and an array of strings
// and returns true if the string matches any array member
function stringInList(searchString, stringList) {
    pino.info('stringInList: string = %s, list = %s', searchString, JSON.stringify(stringList));
    // if no list or empty list then return false
    if (stringList === undefined || stringList === null || stringList.length === 0) {
        pino.info ('stringInList: no list, false');
        return false;
    }
    // iterate through list checking for matches
    for (var stringIndex = 0; stringIndex < stringList.length; stringIndex++) {
        if (searchString === stringList[stringIndex]) {
            pino.info('stringInList: found string %s = list %d %s', searchString, stringIndex, stringList[stringIndex]);
            return true;
        }
    }
    // no matches found, so return false
    pino.info('stringInList: no matches found, false');
    return false;
}

// accountPersonas function raises an appropriate persona panel
// enabling inspection, editing, creation, deletion
// language and res are used to compose panel strings and submit result
// accountRef and permitRef are direct references to required structures
// personaSelection is an optional numeric qualifier
// for the selected/default persona
// note that this panel should make sense even if there are no personas
// and should also reasonably handle stale data
// such as references to attributes no longer present
// !!! old code will not work without refactor for current attribute and persona handling
function accountPersonas(language, accountRef, permitRef, personaSelection, res) {
    // overview: (mostly for debugging)
    // options panel GET /account/personas[/persona]
    // that calls account.personas w/lang, acc, perm, pers, res
    // GET /account/inspect loads accountData (to be filtered by personas)
    // GET /persona/list loads listData with personas for selection menu
    // personaCommitted and personaQualifier derived from personaSelection
    // GET /persona/inspect loads personaData
    // personas structure is derived from personData
    // with .index, .selector, .text, .selected
    // attribute data is generated from account and persona data:
    // with names[], contact[], addresses[], pronouns[]
    // each with .index, .text, .tout, .qualifier, .qout, .toggle, .checked
    // and notes are tacked on at end, to be made attributes
    // ----
    // again revisit 2021-12-17, similar to request/inspect panel
    // selector, attributes with toggles, preview; close, explain
    pino.info('get account personas from account %s, selection %d', accountRef, personaSelection);
    // fetch account data
    var inspectArgs = {
        data: {
            permit: permitRef,
            account: accountRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    service.get(config.accountTender + '/account/inspect', inspectArgs, function (accountData) {
        pino.info('inspection result: %s', JSON.stringify(accountData));
        if (accountData === undefined) {
            pino.info('no inspection data returned');
            // !!! this is incomplete, should return well formed error
            return;
        }
        
        // get persona list
        var listArgs = {
            data: {
                permit: permitRef,
                account: accountRef
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.get(config.accountTender + '/persona/list', listArgs, function(listData) {
            pino.info('persona list result: %s', JSON.stringify(listData));            
            // determine persona selection to use
            // based on given selection and list
            var personaCommitted = 0;
            if (listData.personas.length === 0 || personaSelection === 0) {
                personaCommitted = 0;
            }
            else if (personaSelection <= listData.personas.length) {
                personaCommitted = personaSelection;
            }
            var personaQualifier = listData.personas[personaCommitted];
            
            // generate (fetch and format) persona toggle state data
            // for displayed persona if any, then fill and raise panel
            var personaArgs = {
                data: {
                    permit: permitRef,
                    account: accountRef,
                    qualifier: personaQualifier
                },
                headers: {'Content-Type': 'application/json'}
            };
            pino.info('persona committed index: %d, qualifier: %s', personaCommitted, JSON.stringify(personaQualifier));
            service.get(config.accountTender + '/persona/inspect', personaArgs, function(personaData) {
                pino.info('persona inspection result: %s', JSON.stringify(personaData));
                // !!! REFACTOR IN PROGRESS--INDENTING OFF ON FOLLOWING BLOCKS

        // section, selector, and toggle labels
        let personasText = local.text(language, 'personas');
        let personaSelectText = local.text(language, 'select-persona');
        let toggleText = local.text(language, 'toggle');
        // !!! not clear this personas array is needed or just use listData
        // 
        //  each persona in personas
        //    option(value=persona.selector) #{persona.text}

        var personas = {};
        for (var personaIndex = 0; personaIndex < listData.personas.length; personaIndex++) {
            personas[personaIndex] = {
                index: personaIndex,
                selector: 'persona-' + personaIndex,
                text: listData.personas[personaIndex],
                selected: false
            }
            pino.info('persona construction: index %d, commit %d', personaIndex, personaCommitted);
            // this seems clumsy: selected defaults to false; flip if true
            // and here == works but === does not ?!
            if (personaIndex == personaCommitted) {
                pino.info('personas index %d is comitted %d, selected true', personaIndex, personaCommitted);
                personas[personaIndex].selected = true;
            }
        }
        pino.info('generated personas: %s', JSON.stringify(personas));
                
        // service.get(config.accountTender + '/account/persona', 
        // !!! need to get default persona and its data ...
        //label(id='persona-selector-label') #{persona_selector_label}
        // !!! ROUGHED IN LIST FETCH ONLY USEFUL FOR TESTING LIST FETCH FEATURE
        //     STILL NEED TO BUILD A SELECTION MENU FROM THAT
        
        // attributes: name, contact, address, pronoun
        // names
        let nameText = local.text(language, 'name-label');
        var names = {};
        // !!! CRASHING HERE, NO DATA
        for (var nameIndex = 0; nameIndex < accountData.name.length / 2; nameIndex++) {
            // test if accountData.name[nameIndex *2]
            // is in personaData.name[]
            names[nameIndex] = {
                index: nameIndex,
                text: accountData.name[nameIndex * 2],
                tout: 'name-text-out-' + nameIndex,
                qualifier: accountData.name[nameIndex *2 + 1],
                qout: 'name-qual-out-' + nameIndex,
                toggle: 'name-toggle-' + nameIndex,
                checked: stringInList(accountData.name[nameIndex * 2], personaData.name)
            };
            pino.info('name number %d is %s', nameIndex, JSON.stringify(names[nameIndex]));
        }
        // contacts
        let contactText = local.text(language, 'contact-label');
        var contacts = {};
        for (var contactIndex = 0; contactIndex < accountData.contact.length; contactIndex++) {
            contacts[contactIndex] = {
                index: contactIndex,
                text: accountData.contact[contactIndex * 2],
                tout: 'contact-text-out-' + contactIndex,
                qualifier: accountData.contact[contactIndex *2 + 1],
                qout: 'contact-qual-out-' + contactIndex,
                toggle: 'contacts-toggle-' + contactIndex,
                checked: stringInList(accountData.contact[contactIndex * 2], personaData.contact)
            };
            pino.info('contact number %d is %s', contactIndex, JSON.stringify(contacts[contactIndex]));
        }
        // addresses
        let addressText = local.text(language, 'address-label');
        var addresses = {};
        for (var addressIndex = 0; addressIndex < accountData.address.length; addressIndex++) {
            addresses[addressIndex] = {
                index: addressIndex,
                text: accountData.address[addressIndex * 2],
                tout: 'address-text-out-' + addressIndex,
                qualifier: accountData.address[addressIndex * 2 + 1],
                qout: 'address-qual-out-' + addressIndex,
                toggle: 'address-toggle-' + addressIndex,
                checked: stringInList(accountData.address[addressIndex * 2], personaData.address)
            };
            pino.info('address number %d is %s', addressIndex, JSON.stringify(addresses[addressIndex]));
        }
        // pronouns
        let pronounText = local.text(language, 'pronoun-label');
        var pronouns = {};
        for (var pronounIndex = 0; pronounIndex < accountData.pronoun.length; pronounIndex++) {
            pronouns[pronounIndex] = {
                index: pronounIndex,
                text: accountData.pronoun[pronounIndex * 2],
                tout: 'pronoun-text-out-' + pronounIndex,
                qualifier: accountData.pronoun[pronounIndex * 2 + 1],
                qout: 'pronoun-qual-out-' + pronounIndex,
                toggle: 'pronoun-toggle-' + pronounIndex,
                checked: stringInList(accountData.pronoun[pronounIndex * 2], personaData.pronoun)
            };
            pino.info('pronoun number %d is %s', pronounIndex, JSON.stringify(pronouns[pronounIndex]));
        }

        // notes may not belong on this panel
        let notesText = local.text(language, 'notes-label');
        let notesValueText = accountData.notes;

        // persona action text
        let updateText = local.text(language, 'account-save');
        let deleteText = local.text(language, 'delete');
        let closeText = local.text(language, 'close');
        let newText = local.text(language, 'new-persona');
        let renameText = local.text(language, 'rename-persona');

        // raise account personas panel with all data
        res.render('account-personas', {
            personas_section: personasText,
            persona_selector_label: personaSelectText,
            personas: personas,
            toggle: toggleText,
            name_label: nameText,
            names: names,
            contact_label: contactText,
            contacts: contacts,
            address_label: addressText,
            addresses: addresses,
            pronoun_label: pronounText,
            pronouns: pronouns,
            notes_label: notesText,
            notes_value: notesValueText,
            save: updateText,
            delete_label: deleteText,
            new_persona: newText,
            rename_persona: renameText,
            close: closeText
        });  // end response render block

            });  // end account persona fetch block

        });  // end persona list block

    });  // end account data fetch
}

// respond to 'account-update'
// !!! being refactored on 2021-06-05, accountData may change, starts with notes
//         account.update(socket.request.session.account_ref, socket.request.session.permit, 
// !!! being deprecated in favor of accountPanel with inspect option ??? confirm?
function accountUpdate(accountRef, permitRef, accountData, callback) {
    pino.info('account update');
    var updateArgs = {
        // !!! note need to handle possible new account name not matching permit
        // !!! 2021-06-05 refactor makes necessary data records unclear
        data: {
            account: accountData.account,
            permit: permitRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    if (accountData.notes != undefined & accountData.notes != '') {
        updateArgs.data.notes = accountData.notes;
    }
    pino.info('updating account with data: %s', JSON.stringify(updateArgs));
    service.post(config.accountTender + '/account/update', updateArgs, function(data) {
        pino.info('post update data: %s', JSON.stringify(data));
        callback(data);
    });
}

// respond to 'sign-out'
function accountSignOut(account, permit, callback) {
    var signOutArgs = {
        data: {
            account: account,
            permit: permit
        },
        headers: {'Content-Type': 'application/json'}
    };
    service.post(config.accountTender + '/permit/cancel', signOutArgs, function(data) {
        pino.info('account response data: %s', JSON.stringify(data));
        callback(data);
    });
}

// object for exported routines
var account = {};
// !!! REFACTOR IN PROGRESS
account.request = accountRequest;
account.credential = accountCredential;
account.addCredential = accountAddCredential;
account.complete = accountComplete;
account.references = accountReferences;
account.sign = accountSign;
account.requestChallenge = accountRequestChallenge;
account.challenge = accountChallenge;
account.respond = accountRespond;
account.inspect = accountInspect;
account.summary = accountSummary;
account.personas = accountPersonas;
account.update = accountUpdate;
account.signOut = accountSignOut;
module.exports = account;
