// production manager configuration
var config = {};

// account-tender     51112 stores account and authentication info
config.accountTender = 'http://localhost:51112';

// production-tender
config.proTender = 'http://localhost:51231';

// port for this interface
config.port = '51220';

// expose this config object
module.exports = config;
