// core.js
// essential interface elements
// including root page, message and explain blocks, static pages (about, contact), ...
let pino = require('pino')();
let local = require('./localize');
var core = {};  // object to be exported by module

// root page fetch
//    core.root(req.session.language, req.session.account_ref, req.session.permit, res);
function coreRoot(language, account_ref, permit, res) {
    pino.info('GET / language = %s, account = %s, permit = %s', language, account_ref, permit);
    // prepare text used with both unsigned and signed panels
    let titleText = local.text(language, 'title');
    let languageSelectText = local.text(language, 'language-select');
    let accountSectionText = local.text(language, 'account-section-label');
    let accountResetText = local.text(language, 'account-reset');
    let accountExplainText = local.text(language, 'account-explain');
    let aboutText = local.text(language, 'about');
    let contactText = local.text(language, 'contact');
    // generate the rest based on signed state based on permit
    pino.info('CHECK PERMIT INDICATING SIGNED STATE: %s', permit);
    if (permit === undefined  || permit === null
        || permit === '') {
        // unsigned, no permit
        // unsigned actions: language, sign in, reset, request, explain accounts
        pino.info('unsigned session');
        let accountSignInText = local.text(language, 'account-sign-in');
        let accountRequestText = local.text(language, 'account-request');
        res.render('unsigned', {
            title_text: titleText,
            about_text: aboutText,
            contact_text: contactText,
            language_select_text: languageSelectText,
            // account related
            account_section_text: accountSectionText,
            account_sign_in_text: accountSignInText,
            account_reset_text: accountResetText,
            account_request_text: accountRequestText,
            account_explain_text: accountExplainText
        });
        return;
    }  // end unsigned response block
    else {
        // if not unsigned, then signed
        // signed actions:
        // language, inspect, sign out, reset, delete, explain
        // and select, request, explain for pro elements
        // which are currently person, place, event
        // expected later are item, issue (including review), process, charge, payment
        pino.info('signed session');
        // account related
        let inspectText = local.text(language, 'inspect');
        let personasText = local.text(language, 'personas');
        let accountSignOutText = local.text(language, 'account-sign-out');
        let accountDeleteText = local.text(language, 'account-delete');
        let personSectionText = local.text(language, 'person-section');
        let personExplainText = local.text(language, 'person-explain');
        let placeSectionText = local.text(language, 'place-section');
        let placeExplainText = local.text(language, 'place-explain');
        let eventSectionText = local.text(language, 'event-section');
        let eventExplainText = local.text(language, 'event-explain');
        let createText = local.text(language, 'create');
        let selectText = local.text(language, 'select');
        res.render('signed', {
            title_text: titleText,
            about_text: aboutText,
            contact_text: contactText,
            language_select_text: languageSelectText,
            // account related
            account_section_text: accountSectionText,
            inspect: inspectText,
            personas: personasText,
            account_sign_out_text: accountSignOutText,
            account_reset_text: accountResetText,
            account_delete_text: accountDeleteText,
            // pro elements: person, place, event, and more later
            select: selectText,
            create: createText,
            account_explain_text: accountExplainText,
            person_section: personSectionText,
            person_explain_text: personExplainText,
            place_section: placeSectionText,
            place_explain_text: placeExplainText,
            event_section: eventSectionText,
            event_explain_text: eventExplainText,
        });
        return;
    }  // end permit confirmed, respond signed block
}  // end getRoot block

// display a message, usually result status
function coreMessage(language, subject, res) {  
    pino.info('GET /message/:subject = %s', subject);
    let messageText = local.text(language, subject);
    pino.info('message text: %s', messageText);
    res.render('message', {
        message_text: messageText
    });    
}

// respond to explain request by subject
// takes language, subject, and uses res response parameter
// to respond by rendering explain panel with given strings
function coreExplain(language, subject, res) {
    pino.info('GET /explain/:%s', subject);
    // get translated explanation messages based on subject parameter
    let explainSubjectText = local.text(language, subject + '-subject');
    pino.info('explain subject text: %s', explainSubjectText);
    let explainBodyText = local.text(language, subject + '-body');
    pino.info('explain body text: %s', explainBodyText);
    let closeText = local.text(language, 'close');
    pino.info('close text: %s', closeText);
    res.render('explain', {
        subject: explainSubjectText,
        body: explainBodyText,
        close_text: closeText
    });
}  // end explain panel block

// compose and export module elements
core.root = coreRoot;
core.message = coreMessage;
core.explain = coreExplain;
module.exports = core;
