// event.js -- event related functionality

// internal modules
let config = require('./config');
let local = require('./localize');
let service = require('./service');
let account = require('./account');
let person = require('./person');
let place = require('./place');
// external modules
let pino = require('pino')();
let clientSource = require('node-rest-client').Client;

// eventCreate assembles default data
// and then calls eventInspector to raise a panel
// note that no database records are saved at first
// so event reference is blank
function eventCreate(language, accountRef, permitRef, result) {
    pino.info('create event');
    // compose event record to post and then inspect
    // !!! for now client automatically picks up creator account reference
    let newEventData = {
        data: {
            account: accountRef,
            permit: permitRef,
            client: accountRef,
            language: language
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('data for new event creation: %s', JSON.stringify(newEventData));
    eventPost(newEventData, function(postResult) {
        newEventData.data.ref = postResult.ref;
        eventInspector(language, accountRef, permitRef, newEventData.data, result);
    });
}

// eventOperator shows summaries of event records
// and enables inspection and deletion
// ref is array of refs to events used to indicate selection
// expected is array of times
// !!! what fields should be included in summary not clear
// !!! fails when there are no events to show
function eventOperator(viewLanguage, result, count, ref, summary, expected) {
    // generate translated text for panel
    let languageText = local.text(viewLanguage, 'language');
    let summaryText = local.text(viewLanguage, 'summary');
    let expectedText = local.text(viewLanguage, 'expected');
    let clientText = local.text(viewLanguage, 'client');
    let providerText = local.text(viewLanguage, 'provider');
    let inspectText = local.text(viewLanguage, 'inspect');
    let deleteText = local.text(viewLanguage, 'delete');
    let closeText = local.text(viewLanguage, 'close');
    // !!! form is currently only using summary
    result.render('events', {
        refs: ref,
        expected_label: expectedText,
        expecteds: expected,
        language_label: languageText,
        summary_label: summaryText,
        client_label: clientText,
        provider_label: providerText,
        refs: ref,
        summaries: summary,
        inspect_label: inspectText,
        delete_label: deleteText,
        close: closeText
    });
}

// eventSelector shows summaries of event records
// and enables selection
// which is passed along using a selection structure
// that records the context
function eventSelector(viewLanguage, result, count, eventRef, ref, summary, expected) {
    pino.info('preparing event selector for %d events', count);
    // first row are labels for columns being a subset of most relevant fields
    let languageText = local.text(viewLanguage, 'language');
    let summaryText = local.text(viewLanguage, 'summary');
    let clientText = local.text(viewLanguage, 'client');
    let providerText = local.text(viewLanguage, 'provider');    
    // buttons
    let selectText = local.text(viewLanguage, 'select');
    let closeText = local.text(viewLanguage, 'close');
    // languages, summaries, locations, clients, providers
    // !!! form is currently only using summary
    result.render('eventer', {
        language_label: languageText,
        summary_label: summaryText,
        // PERSON
        client_label: clientText,
        provider_label: providerText,
        refs: ref,
        languages: language,
        summaries: summary,
        clients: client,
        providers: provider,
        select_label: selectText,
        close: closeText
    });
}
// selector: lang, summ, details, location, client, provider
function eventSelect(viewLanguage, accountRef, permitRef, selectionRef, result) {
    pino.info('event select');
    let fetchEventArgs = {
        data: {
            language: viewLanguage,
            account: accountRef,
            permit: permitRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('fetch event args: %s', JSON.stringify(fetchEventArgs));
    service.get(config.proTender + '/event', fetchEventArgs, function(fetchResult) {
        pino.info('event fetch event count: %s', fetchResult.count);
        if (fetchResult.count === 0) {
            return;
        }
        pino.info('event fetch result data: %s', JSON.stringify(fetchResult.data));
        // !!! REFACTOR IN PROGRESS, FIELDS SHOWN ARE CHANGING
        var ref = {};
        var language = {};
        var summary = {};
        //var details = {};
        //var location = {};
        // PERSON
        var client = {};
        var provider = {};
        var expected = {};
        for (var eventIndex = 0; eventIndex < fetchResult.count; eventIndex++) {
            pino.info('event %d: ref=%s, summary=%s', eventIndex,
                fetchResult.data[eventIndex].ref,
                fetchResult.data[eventIndex].summary);
            pino.info('full event data: %s', JSON.stringify(fetchResult.data[eventIndex]));
            ref[eventIndex] = fetchResult.data[eventIndex].ref;
            language[eventIndex] = fetchResult.data[eventIndex].language;
            summary[eventIndex] = fetchResult.data[eventIndex].summary;
            client[eventIndex] = fetchResult.data[eventIndex].client;
            provider[eventIndex] = fetchResult.data[eventIndex].provider;
            expected[eventIndex] = fetchResult.data[eventIndex].expected;
        }
        // viewLanguage, result, count, ref, language, summary, location, client, provider
        // !!! check for fetch errors or emptiness before invoking selector
        if (selectionRef === undefined || selectionRef === null || selectionRef === '') {
            eventOperator(viewLanguage, result, fetchResult.count, ref, summary, expected);
        }
        else {
            //function eventSelector(viewLanguage, result, count, eventRef, ref, summary, expected)
            eventSelector(viewLanguage, result, fetchResult.count, selectionRef, ref, summary, expected);
        }
    });  // end service fetch event data block
}

// inspector
function eventInspector(viewLanguage, accountRef, permitRef, eventRecord, result) {
    pino.info('event inspector: vlang = %s, event = %s', viewLanguage, JSON.stringify(eventRecord));
    // first generate common text
    let sectionText = local.text(viewLanguage, 'event-section');
    let languageText = local.text(viewLanguage, 'language');
    let summaryText = local.text(viewLanguage, 'summary');
    let detailsText = local.text(viewLanguage, 'details');
    // !!! location replaced with person and place
    //let locationText = local.text(viewLanguage, 'locations-label');
    let personText = local.text(viewLanguage, 'person');
    let placeText = local.text(viewLanguage, 'place');
    let selectText = local.text(viewLanguage, 'select');
    let clientText = local.text(viewLanguage, 'client');
    let providerText = local.text(viewLanguage, 'provider');
    let createdText = local.text(viewLanguage, 'created');
    let viewedText = local.text(viewLanguage, 'viewed');
    let modifiedText = local.text(viewLanguage, 'modified');
    let proposedText = local.text(viewLanguage, 'proposed');
    let confirmedText = local.text(viewLanguage, 'confirmed');
    let expectedText = local.text(viewLanguage, 'expected');
    let arrivedText = local.text(viewLanguage, 'arrived');
    let startedText = local.text(viewLanguage, 'started');
    let completedText = local.text(viewLanguage, 'completed');
    // billed, paid, reviewed not yet in panel
    let billedText = local.text(viewLanguage, 'billed');
    let paidText = local.text(viewLanguage, 'paid');
    let reviewedText = local.text(viewLanguage, 'reviewed');
    // button labels
    let saveText = local.text(viewLanguage, 'save');
    let closeText = local.text(viewLanguage, 'close');
    
    person.summary(accountRef, permitRef, eventRecord.person, function(personValue) {
        place.summary(accountRef, permitRef, eventRecord.place, function(placeValue) {
            // !!! account summary currently skipped
            //account.summary(accountRef, permitRef, eventRecord.client, function(clientValue) {
            // !!! INDENTING

    // render inspector
    result.render('event', {
        ref: eventRecord.ref,
        event_section: sectionText,
        language: languageText,
        language_value: eventRecord.language,
        summary: summaryText,
        summary_value: eventRecord.summary,
        details: detailsText,
        details_value: eventRecord.details,
        
        // !!! for all references must get a summary string
        person: personText,
        person_value: personValue,
        person_select: selectText,
        place: placeText,
        place_value: placeValue,
        place_select: selectText,
        // !!! client account summary currently skipped
        //client: clientText,
        //client_value: clientValue,
        provider: providerText,
        provider_value: eventRecord.provider,
        
        viewed: viewedText,
        viewed_value: eventRecord.viewed,
        modified: modifiedText,
        modified_value: eventRecord.modified,
        proposed: proposedText,
        proposed_value: eventRecord.proposed,
        confirmed: confirmedText,
        confirmed_value: eventRecord.confirmed,
        
        expected: expectedText,
        expected_value: eventRecord.expected,
        arrived: arrivedText,
        arrived_value: eventRecord.arrived,
        started: startedText,
        started_value: eventRecord.started,
        completed: completedText,
        completed_value: eventRecord.completed,
        billed: billedText,
        billed_value: eventRecord.billed,
        paid: paidText,
        paid_value: eventRecord.paid,
        reviewed: reviewedText,
        reviewed_value: eventRecord.reviewed,
        
        save: saveText,
        close: closeText
    });  // end result render block
            // !!! client account summary block currently skipped });
        });  // end place summary block
    });  // end person summary block
}

// inspector: language, summary, details, location, 
//   client, provider, completed, paid 
function eventInspect(language, accountRef, permitRef, ref, result) {
    pino.info('event inspect ref=%s', ref);
    let eventFetchArgs = {
        data: {
            language: language,
            account: accountRef,
            permit: permitRef,
            ref: ref
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for event fetch: %s', JSON.stringify(eventFetchArgs));
    service.get(config.proTender + '/event', eventFetchArgs, function(data) {
        pino.info('event fetch result: %s', JSON.stringify(data.data));
        // display inspector with fetched  data
        eventInspector(language, accountRef, permitRef, data.data[0], result);
    });
}

// eventPost saves or updates data about an event
// mostly used to respond to inspector input
function eventPost(eventArgs, callback) {
    service.post(config.proTender + '/event', eventArgs, function(data) {
        pino.info('event post returned: %s', JSON.stringify(data));
        callback(data);
    });
}

// eventSummary generates a short string representing the event
// !!! initial sketch only, not used
function eventSummary(accountRef, permitRef, eventRef, callback) {
    pino.info('generating summary for event %s', eventRef);
    if (eventRef === undefined || eventRef === null || eventRef === '') {
        // empty sea of nothingness
        callback('');
        return;
    }
    let eventFetchArgs = {
        data: {
            account: accountRef,
            permit: permitRef,
            ref: eventRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    service.get(config.proTender + '/event', eventFetchArgs, function(data) {
        // var summary = data.data[0].name;
        // !!! REFACTORING ... WHAT FIELDS?
        // callback(summary);
    });
}

// eventConfirm is the first step of the event delete process
// which raises a panel with a question
function eventConfirm(language, eventRef, result) {
    pino.info('request delete confirm ref=%s', eventRef);
    let confirmText = local.text(language, 'event-delete-confirm');
    let closeText = local.text(language, 'close');
    let deleteText = local.text(language, 'delete');
    result.render('confirm-delete', {
        confirm_text: confirmText,
        close_text: closeText,
        delete_text: deleteText,
        type: 'event',
        ref: eventRef
    });
}

// eventDelete is triggered by an eventConfirm panel
// and does the actual job of removing the selected event
function eventDelete(account, permit, eventRef) {
    let deleteArgs = {
        data: {
            account: account,
            permit: permit,
            ref: eventRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('delete event, data: %s', JSON.stringify(deleteArgs.data));
    service.post(config.proTender + '/event/delete', deleteArgs, function(data) {
        pino.info('event delete returns: %s', JSON.stringify(data));
    });
}

// object for exporting routines
var event = {};
event.create = eventCreate;
// eventOperator, eventSelector
event.select = eventSelect;
// eventInspector
event.inspect = eventInspect;
event.post = eventPost;
// eventSummary
event.confirm = eventConfirm;
event.delete = eventDelete;
module.exports = event;
