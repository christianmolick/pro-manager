'use strict';

// essential application modules
let config = require('./config');
let local = require('./localize');
let core = require('./core');
let account = require('./account');
let person = require('./person');
let place = require('./place');
let item = require('./item');
let issue = require('./issue');
let request = require('./request');
let event = require('./event');
let service = require('./service');  // service calls only used for attribute save, delete
// log with pino
let pino = require('pino')();
// path used for serving static files
let path = require('path');
// fs used for reading translations
let fs = require('fs');
// rest connector for related services
let clientSource = require('node-rest-client').Client;
// express and related including body and cookie parsing
let express = require('express');
let app = express();
let server = require('http').createServer(app);
let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');
// socket connection to application interface
let io = require('socket.io')(server);
// language preference detection
let requestLanguage = require('express-request-language');
let session = require('express-session');
let pgSession = require('connect-pg-simple')(session);
// load fluent and translations for languages
local.init();

// set up express middleware
app.use(cookieParser());  // should not be needed but fails without
app.use(requestLanguage({
    languages: local.supported,
    cookie: {
        name: 'language',
        options: { maxAge: 24*3600*1000 },
        url: '/languages/{language}'
    }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
let sessionMiddleware = session({
    store: new pgSession({
        conString: 'postgres://tender:tdr@localhost:5432/browser_session'
    }),
    secret: 'clearly a missed opportunity here',
    resave: true,
    saveUninitialized: true
});
app.use(sessionMiddleware);
// declare views templates folder and pug as view engine
app.set('views', path.join(__dirname, '../views'));  // path.join gets this wrong?
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, '../public')));
io.use(function(socket, next) {
    sessionMiddleware(socket.request, socket.request.res, next);
});
// prepare to salt and hash
const credentialSalt = 'credential salt';
const { SHA3 } = require('sha3');

// core interface on root page
app.get('/', function (req, res) {
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    core.root(req.session.language, req.session.account_ref, req.session.permit, res);
});

// about and contact are localized static content
// not yet moved to core module because so trivial
app.get('/about', function(req, res) {
    pino.info('GET /about');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    res.sendFile('locales/' + req.session.language + '/about.html', {root: '.'});
});

app.get('/contact', function(req, res) {
    pino.info('GET /contact');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    res.sendFile('locales/' + req.session.language + '/contact.html', {root: '.'});
});

// language options
app.get('/languages', function(req, res) {
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    local.options(req.session.language, res);
});

// fetch posted articles
app.get('/post/:title', function(req, res) {
    pino.info('GET /post/%s', req.params.title);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    res.sendFile('locales/' + req.session.language + '/' + req.params.title + '.html'
        , {root: '.'});
});

// get account request form
app.get('/account/request', function(req, res) {
    pino.info('GET /account/request ');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    account.request(req.session.language, res);
});

app.get('/account/request/:ref', function(req, res) {
    pino.info('GET /account/request/:ref = %s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    account.request(req.session.language, res, req.params.ref);
});

// account credential panel, used by account creation process
app.get('/account/credential/:ref', function(req, res) {
    pino.info('GET /account/credential/:ref, ref = %s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    account.credential(req.session.language, req.params.ref, res);  // ??? callback
});

// get account sign in form
app.get('/account/sign', function(req, res) {
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    account.sign(req.session.language, res);
});

// fetch authentication form
app.get('/account/challenge', function(req, res) {
    pino.info('\n GET /account/challenge');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // language, account_name, permit, res
    account.challenge(req.session.language, req.session.account_ref, req.session.permit, res);
});

app.get('/account/inspect', function(req, res) {
    pino.info('GET /account/inspect name: %s', req.session.account_ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // function accountInspect(language, name ,permit, accountData, res)
    pino.info('inspecting account with lang = %s, acct = %s, permit = %s', 
        req.session.language, req.session.account_ref, req.session.permit);
    account.inspect(req.session.language, req.session.account_ref, req.session.permit, res);
});

// direct inspection stub, may not be needed
// note that in the inspect case the given req.params.ref should match req.session.account_ref
app.get('/account/inspect/:ref', function(req, res) {
    pino.info('GET /account/inspect/:ref = %s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    account.inspect(req.session.language, req.params.ref, req.session.permit, res);
});

// raise personas panel with no specific persona
// first sketch would pick up persona_qualifier from session
// but this was so far never stored ... need to consider and complete
app.get('/account/personas', function(req, res) {
    pino.info('GET /account/personas');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // possibly retain persona in req.session.persona?
    // pass only blank for now, should be ignored because no match
    // note this raises the issue of testing for invalid inputs
    account.personas(req.session.language, req.session.account_ref, req.session.permit, 0, res);
});

// raise persona panel with specified persona
app.get('/account/personas/:persona', function(req, res) {
    pino.info('GET /account/personas, persona = %s', req.params.persona);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // tentative retention of persona in session
    //req.session.persona = req.params.persona;
    //req.session.save(() => {
    //});
    account.personas(req.session.language, req.session.account_ref, req.session.permit, req.params.persona, res);
});

// fetch explain links
// no language parameter in explain fetch as it uses session language
app.get('/explain/:subject', function(req, res) {
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    core.explain(req.session.language, req.params['subject'], res);
});

// display a message, usually result of an operation
app.get('/message/:subject', function(req, res) {  
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    core.message(req.session.language, req.params['subject'], res);
});

// person related routing: select, create, inspect, delete
app.get('/person/select', function(req, result) {
    pino.info('GET /person/select');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // !!! place version has two nulls before result?
    person.select(req.session.language, req.session.account_ref, req.session.permit, null, result)
});

// person select with reference
// uses the selection panel with a selection record
app.get('/person/select/:ref', function(req, result) {
    pino.info('GET /person/select/:ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // call place select with essential data
    // !!! note place select place ref is not used and should be removed?
    //function personSelect(viewLanguage, accountRef, permitRef, selectionRef, result) 
    person.select(req.session.language, req.session.account_ref, req.session.permit, req.params.ref, result); 
});


// person creation raises an inspector 
app.get('/person/create', function(req, result) {
    pino.info('GET /person/create');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    person.create(req.session.language,
        req.session.account_ref, req.session.permit, result);
});

app.get('/person/inspect/:ref', function(req, result) {
    pino.info('GET /person/inspect/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    person.inspect(req.session.language,
        req.session.account_ref, req.session.permit,
        req.params.ref, result);
});

// person delete button is on person selector, this is the confirmation panel
app.get('/person/delete/:ref', function(req, result) {
    pino.info('GET /person/delete/ref, ref=%s', req.params['ref']);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // !!! remove account reference?
    person.confirm(req.session.language, req.session.account_ref, req.params.ref, result);
});

// place related calls: select, create, inspect, delete
// place select raises a selector tool for search/browse of existing visible records
app.get('/place/select', function(req, result) {
    pino.info('GET /place/select');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // !!! note place select place ref is not used and should be removed?
    place.select(req.session.language, req.session.account_ref, req.session.permit, null, null, result);
});

// place select with reference
// uses the selection panel with a selection record
app.get('/place/select/:ref', function(req, result) {
    pino.info('GET /place/select/:ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    // call place select with language, account, permit, place, null selection record, and result
    // the null selection record means this is select from all to inspect or delete, not for choice
    // !!! note place select place ref is not used and should be removed?
    place.select(req.session.language, req.session.account_ref, req.session.permit, null, req.params.ref, result); 
});

// person creation raises an inspector for a default place
// this is get instead of post because it recalls a form only
app.get('/place/create', function(req, result) {
    pino.info('GET /place/create');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    //function placeCreate(language, accountRef, permitRef, result)
    place.create(req.session.language,
        req.session.account_ref, req.session.permit, result);
});

// place inspect shows records of specified place
app.get('/place/inspect/:ref', function(req, result) {
    pino.info('GET /place/inspect/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    //place.inspect(req.session.language,
    //    req.session.account_ref, req.session.permit,
    //    req.params.ref, result);
    // !!! interface has changed, must refactor
    // !!! permit is not being checked in this case
    //function placeInspect(language, account, permit, ref, result) {
    place.inspect(req.session.language, req.session.account_ref, req.session.permit,  req.params.ref, result);
    //    req.session.account_ref, req.session.permit,
    //    req.params.ref, result);
});

// person delete button is on person selector, this is the confirmation panel
// this is get instead of post because it recalls a form only
app.get('/place/delete/:ref', function(req, result) {
    pino.info('GET /place/delete/:ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    place.confirm(req.session.language, req.params.ref, result);
});


// event related: select, create, inspect, delete 
app.get('/event/select', function(req, result) {
    pino.info('GET /event/select');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    //function eventSelect(viewLanguage, accountRef, permitRef, selectionRef, result)
    event.select(req.session.language, req.session.account_ref, req.session.permit, null, result)
});

// GET /event/select/:ref

app.get('/event/create', function(req, result) {
    pino.info('GET /event/create');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    event.create(req.session.language,
        req.session.account_ref, req.session.permit, result);
});

app.get('/event/inspect/:ref', function(req, result) {
    pino.info('GET /event/inspect/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    event.inspect(req.session.language,
        req.session.account_ref, req.session.permit,
        req.params.ref, result);
});

// event delete button is on event selector, this is the confirmation panel
app.get('/event/delete/:ref', function(req, result) {
    pino.info('GET /event/delete/ref, ref=%s', req.params['ref']);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    event.confirm(req.session.language, req.params.ref, result);
});

// essential item supports
// item related calls: select, create, inspect, delete
app.get('/item/select', function(req, result) {
    pino.info('GET /item/select');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    item.select(req.session.language, req.session.account_ref, req.session.permit, result)
});

// item creation raises an inspector 
app.get('/item/create', function(req, result) {
    pino.info('GET /item/create');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    item.create(req.session.language,
        req.session.account_name, req.session.permit, result);
});

app.get('/item/inspect/:ref', function(req, result) {
    pino.info('GET /item/inspect/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    item.inspect(req.session.language,
        req.session.account_ref, req.session.permit,
        req.params.ref, result);
});

// person delete button is on person selector, this is the confirmation panel
app.get('/item/delete/:ref', function(req, result) {
    pino.info('GET /item/delete/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    item.confirm(req.session.language, req.params.ref, result);
});

// issues
// essential issue supports
// issue related calls: select, create, inspect, delete
app.get('/issue/select', function(req, result) {
    pino.info('GET /issue/select');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    issue.select(req.session.language, req.session.account_ref, req.session.permit, result)
});

// issue creation raises an inspector 
app.get('/issue/create', function(req, result) {
    pino.info('GET /issue/create');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    issue.create(req.session.language,
        req.session.account_ref, req.session.permit, result);
});

app.get('/issue/inspect/:ref', function(req, result) {
    pino.info('GET /issue/inspect/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    issue.inspect(req.session.language,
        req.session.account_ref, req.session.permit,
        req.params.ref, result);
});

// delete button is on selector, this is the confirmation panel
app.get('/issue/delete/:ref', function(req, result) {
    pino.info('GET /issue/delete/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    issue.confirm(req.session.language, req.params.ref, result);
});


// request creation raises an inspector 
app.get('/request/create', function(req, result) {
    pino.info('GET /request/create');
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    request.create(req.session.language,
        req.session.account_ref, req.session.permit, result);
});

app.get('/request/inspect/:ref', function(req, result) {
    pino.info('GET /request/inspect/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    request.inspect(req.session.language,
        req.session.account_ref, req.session.permit,
        req.params.ref, result);
});

// delete button is on selector, this is the confirmation panel
app.get('/request/delete/:ref', function(req, result) {
    pino.info('GET /request/delete/ref, ref=%s', req.params.ref);
    if (req.session.language === undefined) {
        req.session.language = req.language;
    }
    request.confirm(req.session.language, req.params.ref, result);
});

// production error handler                                                    
// no stacktraces leaked to user       
// req and res completely dropped, only errors interesting
app.use(function (err) {  // next?
    app.render('error', {
        message: err.message,
        error: {}
    });
});  // end error handler block

// javascript event responses, mostly forms
io.on('connection', function(socket) {
    pino.info('connection');
    socket.on('error', function(err) {
        // basic error response should only happen with communication failure
        pino.error('socket connection error: %s', err);
    });
    socket.on('disconnect', function() {
        pino.info('disconnected');
    });
    // basic interface events
    socket.on('language-select', function(lang) {
        // select a language from list on language panel
        pino.info('new language, raw code: %s', lang);
        // to be generated by button on languages panel
        socket.request.session.language = lang;
        socket.request.session.save(() => {
            io.emit('redisplay');
            // display changed to language message?
        });
    }); // end language selection message block
    socket.on('request-account', function(message) {
        // send account application from create panel
        pino.info('request account: %s', JSON.stringify(message));
    });  // end request account block
    socket.on('request-challenge', function(message) {
        // authenticate named account by fetching challenges and responding
        pino.info('request challenge with inputs = %s', JSON.stringify(message));
        account.references(message.name, message.contact, message.address, message.pronoun, function(refs) {
            // store asserted account name
            socket.request.session.account_ref = refs.account;
            account.requestChallenge(refs.account, function (data) {
                pino.info('challenge fetch result: %s, challenge: %s', data.result, data.challenge);
                if (data === undefined || data.result == 'failed' || data.challenge === undefined) {
                    // data undefined means account missing/blocked
                    pino.info('get challenge failed');
                    io.emit('message', 'sign-in-failure');
                    return;
                }
                socket.request.session.save(() => {
                    pino.info('challenge save callback: challenge');
                    io.emit('challenge', data.challenge);
                    io.emit('message', 'sign-in-challenge');
                });  // end session save block
            });  // end request challenge block
        });  // end account references fetch block
    });  // end challenge request block
    socket.on('submit-response', function(message) {
        // submit response as composed for last given challenge
        // message has account (ref) and response
        pino.info('submitting response: %s', JSON.stringify(message));
        account.respond(message.account, message.response, function (data) {
            pino.info('response result: %s', JSON.stringify(data));
            if (data.result === 'okay' ) {
                if (data.challenge === undefined) {
                    // correct and final response so sign in
                    pino.info('response okay, signing in');
                    socket.request.session.account_ref = data.ref;
                    socket.request.session.permit = data.permit;  // permit indicates signed
                    // name is no longer stored and permit must be supplied for all actions
                    socket.request.session.save(() => {
                        io.emit('redisplay');
                        io.emit('message', 'sign-in-success');
                    });
                }
                else {
                    // correct response, next challenge
                    pino.info('response okay, next challenge');
                    io.emit('challenge', data.challenge);
                    // also need redisplay, other?
                }
             }
            else {
                pino.info('response rejected, sign in failed');
                //io.emit('redisplay');
                io.emit('message', 'sign-in-failed');
            }
        });
    });
    socket.on('account-update', function(message) {
        pino.info('account-update message: %s', JSON.stringify(message));
        let accountRecord = {
            permit: socket.request.session.permit,
            account: socket.request.session.account_ref,
            language: socket.request.session.language
        };
        if (message.notes != undefined & message.notes != '') {
            accountRecord.notes = message.notes;
        }
        // function accountUpdate(account_name, permit, accountData, callback) {
        account.update(socket.request.session.account_ref, socket.request.session.permit, 
            accountRecord, function (data) {
            io.emit('message', 'account-updated');
        });
    });
    // name attribute save and delete actions
    socket.on('attribute-delete', function(message) {
        pino.info('attribute delete, message: %s', JSON.stringify(message));
        let deleteArgs = {
            data: {
                account: message.account,
                type: message.type,
                value: message.value,
                qualifier: message.qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/delete', deleteArgs, function(deleteResult) {
            if (deleteResult === undefined || deleteResult.result === 'failed') {
                // report failure and gracefully return?
                return;
            }
            io.emit(message.redisplay, message.account);  // reload panel with /account/request
        });
    });
    socket.on('attribute-add', function(message) {
        // message parameters: account, type, value, qualifier
        pino.info('attribute add, message: %s', JSON.stringify(message));
        let addArgs = {
            data: {
                account: message.account,
                type: message.type,
                value: message.value,
                qualifier: message.qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', addArgs, function(addResult) {
            if (addResult.result === 'failed') {
                // report failure and gracefully return?
                return;
            }
            io.emit(message.redisplay, message.account);  // reload panel with /account/request
        });
    });
    socket.on('attribute-modify', function(message) {
        pino.info('attribute modify, message: %s', JSON.stringify(message));
        // modify attribute entry by looping through cookie attributes
        // and changing the one specific entry
        let deleteArgs = {
            data: {
                //account: socket.request.session.account_ref,
                account: message.account,
                type: message.type,
                value: message.value,
                qualifier: message.qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('attribute-modify delete args: %s', JSON.stringify(deleteArgs));
        service.post(config.accountTender + '/attribute/delete', deleteArgs, function(deleteResult) {
            if (deleteResult.result === 'failed') {
                // report failure and gracefully return?
                return;
            }
            pino.info('attribute-modify delete result: %s', JSON.stringify(deleteResult));

        let addArgs = {
            data: {
                //account: socket.request.session.account_ref,
                account: message.account,
                type: message.type,
                value: message.value,
                qualifier: message.qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        if (message.newtype != undefined && message.newtype != '') {
            addArgs.data.type = message.newtype;
        }
        if (message.newvalue != undefined && message.newvalue != '') {
            addArgs.data.value = message.newvalue;
        }
        if (message.newqualifier != undefined && message.newqualifier != '') {
            addArgs.data.qualifier = message.newqualifier;
        }
        pino.info('attribute-modify add args: %s', JSON.stringify(addArgs));
        service.post(config.accountTender + '/attribute/save', addArgs, function(addResult) {
            if (addResult.result === 'failed') {
                // report failure and gracefully return?
                return;
            }
            pino.info('attribute-modify add args result: %s', JSON.stringify(addResult));
            io.emit(message.redisplay, message.account);  // reload panel with /account/request
        });  // end save attribute block
        
        });  // end delete attribute block
        
    });  // end attribute modify message response block
    socket.on('request-complete', function(message) {
        pino.info('request complete, message: %s', JSON.stringify(message));
        // salt and hash inputs
        // should confirm that required name, language, and contact attributes are present
        // make account with all given attribute and credential input
        // !!! check for undefined, null, invalid account reference
        account.complete(message.account, message.challenge, message.response, message.confirm, function(result) {
            // dump result
            // if complete succeeded then redisplay app panel, otherwise back to credential panel
            // !!! check for failure
            pino.info('account complete result: %s', JSON.stringify(result))
            io.emit('redisplay');
            // raise message with result 
        });
    });
    // add challenge is used to add another challenge and response
    // or email code to authentication
    //  $('request-another').on('click', function() 
    socket.on('request-another', function(message) {
        pino.info('request another, message: %s', JSON.stringify(message));
        // make sure attributes are stored and move on to credential input
        // !!! check for undefined, null, invalid account reference
        account.addCredential(message.account, message.challenge, message.response, message.confirm, function(result) {
            // !!! check for failure
            // based on result 
            // if credential accepted then okay, either way raise credential panel
            pino.info('result of add credential: %s', JSON.stringify(result));
            pino.info('emitting redisplay credential message');
            io.emit('credential-redisplay', message.account);
            pino.info('emitted redisplay credential message');
        });
    });   
    // add challenge is used to add another challenge and response
    // or email code to authentication
    socket.on('email-code-toggle', function(message) {
        pino.info('email code toggle, message: %s', JSON.stringify(message));
        // make sure attributes are stored and move on to credential input
        
    });   
    socket.on('name-save', function(message) {
        pino.info('name-save: message = %s', JSON.stringify(message));
        var nameSaveArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'name',
                old_text: message.old_text,
                new_text: message.new_text,
                old_qualifier: message.old_qualifier,
                new_qualifier: message.new_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', nameSaveArgs, function(data) {
            pino.info('name attribute save data: %s', JSON.stringify(data));
        });
    });
    socket.on('name-delete', function(message) {
        pino.info('name-delete: message = %s', JSON.stringify(message));
        var nameDeleteArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'name',
                index: message.index,
                old_text: message.old_text,
                old_qualifier: message.old_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/delete', nameDeleteArgs, function(data) {
            pino.info('name attribute delete data: %s', JSON.stringify(data));
        });
    });
    socket.on('new-name-save', function(message) {
        pino.info('new-name-save: message = %s', JSON.stringify(message));
        var newNameSaveArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'name',
                new_text: message.new_text,
                new_qualifier: message.new_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', newNameSaveArgs, function(data) {
            pino.info('new attribute data: %s', JSON.stringify(data));
        });
    });
    // contact attribute save and delete actions
    socket.on('contact-save', function(message) {
        pino.info('contact-save: message = %s', JSON.stringify(message));
        var contactSaveArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'contact',
                old_text: message.old_text,
                new_text: message.new_text,
                old_qualifier: message.old_qualifier,
                new_qualifier: message.new_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', contactSaveArgs, function(data) {
            pino.info('contact attribute save data: %s', JSON.stringify(data));
        });
    });
    socket.on('contact-delete', function(message) {
        pino.info('contact-delete: message = %s', JSON.stringify(message));
        var contactDeleteArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'contact',
                index: message.index,
                old_text: message.old_text,
                old_qualifier: message.old_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/delete', contactDeleteArgs, function(data) {
            pino.info('contact attribute delete data: %s', JSON.stringify(data));
        });
    });
    socket.on('new-contact-save', function(message) {
        pino.info('new-contact-save: message = %s', JSON.stringify(message));
        var newContactSaveArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'contact',
                new_text: message.new_text,
                new_qualifier: message.new_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', newContactSaveArgs, function(data) {
            pino.info('new contact attribute data: %s', JSON.stringify(data));
        });
    });
    // address attribute save and delete actions
    socket.on('address-save', function(message) {
        pino.info('address-save: message = %s', JSON.stringify(message));
        var addressSaveArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'address',
                old_text: message.old_text,
                new_text: message.new_text,
                old_qualifier: message.old_qualifier,
                new_qualifier: message.new_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', addressSaveArgs, function(data) {
            pino.info('address attribute save data: %s', JSON.stringify(data));
        });
    });
    socket.on('address-delete', function(message) {
        pino.info('address-delete: message = %s', JSON.stringify(message));
        var addressDeleteArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'address',
                index: message.index,
                old_text: message.old_text,
                old_qualifier: message.old_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/delete', addressDeleteArgs, function(data) {
            pino.info('address attribute delete data: %s', JSON.stringify(data));
        });
    });
    socket.on('new-address-save', function(message) {
        pino.info('new-address-save: message = %s', JSON.stringify(message));
        var newAddressSaveArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'address',
                new_text: message.new_text,
                new_qualifier: message.new_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', newAddressSaveArgs, function(data) {
            pino.info('new address attribute data: %s', JSON.stringify(data));
        });
    });
    // pronoun attribute save and delete actions
    socket.on('pronoun-save', function(message) {
        pino.info('pronoun-save: message = %s', JSON.stringify(message));
        var pronounSaveArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'pronoun',
                old_text: message.old_text,
                new_text: message.new_text,
                old_qualifier: message.old_qualifier,
                new_qualifier: message.new_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', pronounSaveArgs, function(data) {
            pino.info('pronoun attribute save data: %s', JSON.stringify(data));
        });
    });
    socket.on('pronoun-delete', function(message) {
        pino.info('pronoun-delete: message = %s', JSON.stringify(message));
        var pronounDeleteArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'pronoun',
                index: message.index,
                old_text: message.old_text,
                old_qualifier: message.old_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/delete', pronounDeleteArgs, function(data) {
            pino.info('pronoun attribute delete data: %s', JSON.stringify(data));
        });
    });
    socket.on('new-pronoun-save', function(message) {
        pino.info('new-pronoun-save: message = %s', JSON.stringify(message));
        var newPronounSaveArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                attribute: 'pronoun',
                new_text: message.new_text,
                new_qualifier: message.new_qualifier
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/attribute/save', newPronounSaveArgs, function(data) {
            pino.info('new pronoun attribute data: %s', JSON.stringify(data));
        });
    });
    socket.on('name-toggle', function(message) {
        pino.info('name-toggle, message = %s', JSON.stringify(message));
        var nameToggleArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                qualifier: message.qualifier,
                attribute: 'name',
                text: message.text,
                state: message.checked
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('name toggle data to post: %s', JSON.stringify(nameToggleArgs));
        service.post(config.accountTender + '/persona/toggle', nameToggleArgs, function(data) {
            pino.info('persona name toggle result: %s', JSON.stringify(data));
        });
    });
    socket.on('contact-toggle', function(message) {
        pino.info('contact-toggle, message = %s', JSON.stringify(message));
            var contactToggleArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                qualifier: message.qualifier,
                attribute: 'contact',
                text: message.text,
                state: message.checked
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('contact toggle data to post: %s', JSON.stringify(contactToggleArgs));
        service.post(config.accountTender + '/persona/toggle', contactToggleArgs, function(data) {
            pino.info('persona contact toggle result: %s', JSON.stringify(data));
        });

    });
    socket.on('address-toggle', function(message) {
        pino.info('address-toggle, message = %s', JSON.stringify(message));
            var addressToggleArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                qualifier: message.qualifier,
                attribute: 'address',
                text: message.text,
                state: message.checked
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('address toggle data to post: %s', JSON.stringify(addressToggleArgs));
        service.post(config.accountTender + '/persona/toggle', addressToggleArgs, function(data) {
            pino.info('persona address toggle result: %s', JSON.stringify(data));
        });

    });
    socket.on('pronoun-toggle', function(message) {
        pino.info('pronoun-toggle, message = %s', JSON.stringify(message));
        var pronounToggleArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                qualifier: message.qualifier,
                attribute: 'pronoun',
                text: message.text,
                state: message.checked
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('pronoun toggle data to post: %s', JSON.stringify(pronounToggleArgs));
        service.post(config.accountTender + '/persona/toggle', pronounToggleArgs, function(data) {
            pino.info('persona pronoun toggle result: %s', JSON.stringify(data));
        });
    });
    // !!! all persona related features are initial sketch only
    // persona select from the list generated popup menu
    socket.on('persona-select', function(message) {
        pino.info('persona-select, message = %s', JSON.stringify(message));
    });
    // persona save to be deprecated
    socket.on('persona-save', function(message) {
        pino.info('persona-save, message = %s', JSON.stringify(message));
        // ...
    });
    socket.on('persona-delete', function(message) {
        pino.info('persona-delete, message = %s', JSON.stringify(message));
        // ...
    });
    socket.on('persona-new', function(message) {
        pino.info('persona-new, message = %s', JSON.stringify(message));
        // any initial name, contact, address, pronoun ... and notes?
        var newPersonaArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                qualifier: message.new_name
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.accountTender + '/persona/request', newPersonaArgs, function(data) {
            pino.info('new persona request result: %s', JSON.stringify(data));
        });
        // need to refresh persona panel with new entry
        // possibly need to bring to front first
    });
    socket.on('persona-rename', function(message) {
        pino.info('persona-rename, message = %s', JSON.stringify(message));
        // ... account tender /persona/update with qualifier should work ...
    });

    socket.on('sign-out', function() {
        pino.info('sign-out');
        // end session and invalidate authentication
        account.signOut(socket.request.session.account_ref, socket.request.session.permit, function(data) {
            pino.info('sign out returned: %s', JSON.stringify(data));
            // report success and let failures be silent?!
            if (data.result === 'done') {
                socket.request.session.account_ref = '';
                socket.request.session.account_name = '';
                socket.request.session.account_full_name = '';
                socket.request.session.permit = '';  // date/time?
                socket.request.session.save(() => {
                    pino.info('signed out session: %s', socket.request.session);
                    io.emit('redisplay');
                });
            }
        });
    });    
    socket.on('person-container-select', function(message) {
        // initiate selection process: post selection record, call for selector panel
        pino.info('person-container-select: %s', JSON.stringify(message));
        // make transaction record on pro tender
        let processArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                context: message.context,
                context_type: message.context_type,
                field_type: message.field_type,
                field_name: message.field_name
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.proTender + '/selection', processArgs, function(postResult) {
            pino.info('person-container-select person data post result: %s', JSON.stringify(postResult));
            // check for success and fail gracefully if necessary
            io.emit('person-select', postResult.ref);
            // !!! not clear if selection should be updated or just deleted
        
            // send message that raises place inspector for this transaction
            // place-selection message to signed
            // which loads /place/select/ref
        });
    });
    socket.on('person-selected', function(message) {
        // selection made: record it, delete selection process record, raise context panel
        pino.info('person selected: %s', JSON.stringify(message));
        // recall necessary information from selection record
        let selectionFetchArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.get(config.proTender + '/selection/' + message.selection, selectionFetchArgs, function(fetchResult) {
            // selection fetch should have context, context_type, field_type, field_name
            pino.info('selection fetch result %s', JSON.stringify(fetchResult));
            // !!! post either person or event depending on context
            if (fetchResult.context_type === 'person') {
                // use selection data to record the data
                // check that field_type is place and field_name is container? no other option?
                let recordSelectionArgs = {
                    data : {
                        account: socket.request.session.account_ref,
                        permit: socket.request.session.permit,
                        ref: fetchResult.context,
                        container: message.selected
                    },
                    headers: {'Content-Type': 'application/json'}
                };
                pino.info('record selection args: %s', JSON.stringify(recordSelectionArgs));
                person.post(recordSelectionArgs, function(recordResult) {
                    // check and report results
                    pino.info('selection recording result: %s', JSON.stringify(recordResult));
                    // delete no longer needed selection record
                    // call for inspector/context to be raised with place reference
                    io.emit('person-inspect', fetchResult.context);
                });  // end post selection block
            }  // end if fetch result context type is person block
            else if (fetchResult.context_type === 'event') {
                let recordSelectionArgs = {
                    data : {
                        account: socket.request.session.account_ref,
                        permit: socket.request.session.permit,
                        ref: fetchResult.context,
                        person: message.selected
                    },
                    headers: {'Content-Type': 'application/json'}
                };
                pino.info('record selection args: %s', JSON.stringify(recordSelectionArgs));
                event.post(recordSelectionArgs, function(recordResult) {
                    // check and report results
                    pino.info('selection recording result: %s', JSON.stringify(recordResult));
                    // delete no longer needed selection record
                    // call for inspector/context to be raised with place reference
                    io.emit('event-inspect', fetchResult.context);
                });  // end post selection block
                
            }
            else {
                // context not recognized, so fail gracefully
                pino.info('person selection context unrecognized, failing');
            }
        });  // end fetch selection block
    });

    socket.on('person-save', function(message) {
        // complete new person process
        pino.info('person-save: %s', JSON.stringify(message)); 
        // name, full_name, language, notes
        var personName;
        if (message['name_input'] === undefined || message['name_input'] === '' ) {
            personName = message['name_value'];
        }
        else {
            personName = message['name_input'];
        }
        var personFullName;
        if (message['full_name_input'] === undefined || message['full_name_input'] === '' ) {
            personFullName = message['full_name_value'];
        }
        else {
            personFullName = message['full_name_input'];
        }
        var personLanguage;
        if (message['language_input'] === undefined || message['language_input'] === '' ) {
            personLanguage = message['language_value'];
        }
        else {
            personLanguage = message['language_input'];
        }
        var personProfile;
        if (message['profile_input'] === undefined || message['profile_input'] === '' ) {
            personProfile = message['profile_value'];
        }
        else {
            personProfile = message['profile_input'];
        }
        var personPreferences;
        if (message['preferences_input'] === undefined || message['preferences_input'] === '' ) {
            personPreferences = message['preferences_value'];
        }
        else {
            personPreferences = message['preferences_input'];
        }
        var personProvides;
        if (message['provides_input'] === undefined || message['provides_input'] === '' ) {
            personProvides = message['provides_value'];
        }
        else {
            personProvides = message['provides_input'];
        }
        let personArgs = {
            data: {
                permit: socket.request.session.permit,
                account: socket.request.session.account_ref,
                ref: message['ref_value'],
                name: personName,
                full_name: personFullName,
                language: personLanguage,
                profile: personProfile,
                preferences: personPreferences,
                provides: personProvides
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('pino post person data: %s', JSON.stringify(personArgs.data));
        person.post(personArgs, function(saveResult) {
            pino.info('person save result: %s', saveResult);
            // fail gracefully if error? other tasks needed?
        });
    });
    socket.on('confirmed-delete', function(message) {
        pino.info('confirmed delete, message: %s', JSON.stringify(message));
        if (message.type === 'person') {
            person.delete(socket.request.session.account_ref, socket.request.session.permit, message.ref);
        }
        else if (message.type === 'place') {
            place.delete(socket.request.session.account_ref, socket.request.session.permit, message.ref);
        }
        else if (message.type === 'event') {
            event.delete(socket.request.session.account_ref, socket.request.session.permit, message.ref);
        }
        else if (message.type === 'item') {
            item.delete(socket.request.session.account_ref, socket.request.session.permit, message.ref);
        }
        else if (message.type === 'issue') {
            issue.delete(socket.request.session.account_ref, socket.request.session.permit, message.ref);
        }
        else if (message.type === 'request') {
            request.delete(socket.request.session.account_ref, socket.request.session.permit, message.ref);
        }
        else {
            pino.error('confirmed deletion of unknown unit type');
        }
    });
    // !!! replaced by place-selected
    socket.on('place-select', function(message) {
        pino.info('place-select: %s', JSON.stringify(message));
        // make transaction record on pro tender
        // send message that raises place inspector for this transaction
    });
    // !!! place selection refactor in progress
    // these routines might best be joined as one
    
    socket.on('place-container-select', function(message) {
        // initiate selection process: post selection record, call for selector panel
        pino.info('place-container-select: %s', JSON.stringify(message));
        // make transaction record on pro tender
        let processArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                context: message.context,
                context_type: message.context_type,
                field_type: message.field_type,
                field_name: message.field_name
            },
            headers: {'Content-Type': 'application/json'}
        };
        service.post(config.proTender + '/selection', processArgs, function(postResult) {
            pino.info('place-container-select place data post result: %s', JSON.stringify(postResult));
            // check for success and fail gracefully if necessary
            io.emit('place-select', postResult.ref);
            // !!! not clear if selection should be updated or just deleted

            // send message that raises place inspector for this transaction
            // place-selection message to signed
            // which loads /place/select/ref
        });
    });
    
    socket.on('place-selected', function(message) {
        // selection made: record it, delete selection process record, raise context panel
        pino.info('place selected: %s', JSON.stringify(message));
        // recall necessary information from selection record
        let selectionFetchArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit
            },
            headers: {'Content-Type': 'application/json'}
        };
        //pino.info('fetch selection data %s', JSON.stringify(placeArgs.data));
        service.get(config.proTender + '/selection/' + message.selection, selectionFetchArgs, function(fetchResult) {
            // selection fetch should have context, context_type, field_type, field_name
            pino.info('selection fetch result %s', JSON.stringify(fetchResult));
            // use selection data to record the data
            if (fetchResult.context_type === 'place') {
                let recordSelectionArgs = {
                    data : {
                        account: socket.request.session.account_ref,
                        permit: socket.request.session.permit,
                        ref: fetchResult.context,
                        container: message.selected
                        },
                    headers: {'Content-Type': 'application/json'}
                };
                pino.info('record selection args: %s', JSON.stringify(recordSelectionArgs));
                place.post(recordSelectionArgs, function(recordResult) {
                    // check and report results
                    pino.info('selection recording result: %s', JSON.stringify(recordResult));
                    // delete no longer needed selection record
                    // call for inspector/context to be raised with place reference
                    io.emit('place-inspect', fetchResult.context);
                });  // end post selection block
            }
            else if (fetchResult.context_type === 'event') {
                let recordSelectionArgs = {
                    data : {
                        account: socket.request.session.account_ref,
                        permit: socket.request.session.permit,
                        ref: fetchResult.context,
                        place: message.selected
                    },
                    headers: {'Content-Type': 'application/json'}
                };
                pino.info('record selection args: %s', JSON.stringify(recordSelectionArgs));
                event.post(recordSelectionArgs, function(recordResult) {
                    // check and report results
                    pino.info('selection recording result: %s', JSON.stringify(recordResult));
                    // delete no longer needed selection record
                    // call for inspector/context to be raised with place reference
                    io.emit('event-inspect', fetchResult.context);
                });  // end post selection block
            }
            else {
                // unknown type of selection, so fail gracefully
                pino.info('unknown selection type %s', fetchResult);
            }
        });  // end fetch selection block
       
    });
    socket.on('place-save', function(message) {
        // complete new place process by saving data from message
        pino.info('place-save: %s', JSON.stringify(message)); 
        // pick up new values if possible or use older given values
        var placeName;
        if (message['name_input'] === undefined || message['name_input'] === '' ) {
            placeName = message['name_value'];
        }
        else {
            placeName = message['name_input'];
        }
        var placeFullName;
        if (message['full_name_input'] === undefined || message['full_name_input'] === '' ) {
            placeFullName = message['full_name_value'];
        }
        else {
            placeFullName = message['full_name_input'];
        }
        var placeLanguage;
        if (message['language_input'] === undefined || message['language_input'] === '' ) {
            placeLanguage = message['language_value'];
        }
        else {
            placeLanguage = message['language_input'];
        }
        var placeType;
        if (message['type_input'] === undefined || message['type_input'] === '' ) {
            placeType = message['type_value'];
        }
        else {
            placeType = message['type_input'];
        }
        var placeDescription;
        if (message['description_input'] === undefined || message['description_input'] === '' ) {
            placeDescription = message['description_value'];
        }
        else {
            placeDescription = message['description_input'];
        }
        var placeNotes;
        if (message['notes_input'] === undefined || message['notes_input'] === '' ) {
            placeNotes = message['notes_value'];
        }
        else {
            placeNotes = message['notes_input'];
        }
        let placeArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                ref: message['ref'],
                language: placeLanguage,
                name: placeName,
                full_name: placeFullName,
                type: placeType,
                description: placeDescription,
                notes: placeNotes
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('pino post place data %s', JSON.stringify(placeArgs.data));
        place.post(placeArgs, function (result) {
            pino.info('pino post place result %s', JSON.stringify(result));
        });
    });
    socket.on('event-save', function(message) {
        // complete event post process
        pino.info('event-save: %s', JSON.stringify(message)); 
        // pick up new values if given
        var eventLanguage;
        if (message['language_input'] === '' ) {
            eventLanguage = message['language_default'];
        }
        else {
            eventLanguage = message['language_input'];
        }
        var eventSummary;
        if (message['summary_input'] === '' ) {
            eventSummary = message['summary_default'];
        }
        else {
            eventSummary = message['summary_input'];
        }
        var eventDetails;
        if (message['details_input'] === '' ) {
            eventDetails = message['details_default'];
        }
        else {
            eventDetails = message['details_input'];
        }
        var eventClient;
        if (message['client_input'] === undefined || message['client_input'] === '' ) {
            eventClient = message['client_default'];
        }
        else {
            eventClient = message['client_input'];
        }
        var eventProvider;
        if (message['provider_input'] === undefined || message['provider_input'] === '' ) {
            eventProvider = message['provider_default'];
        }
        else {
            eventProvider = message['provider_input'];
        }
        var eventExpected;
        if (message.expected_input === '') {
            eventExpected = message.expected_default;
        }
        else {
            eventExpected = message.expected_input;
        }
        var eventCompleted;
        if (message['completed_input'] === undefined || message['completed_input'] === '' ) {
            eventCompleted = message['completed_default'];
        }
        else {
            eventCompleted = message['completed_input'];
        }
        var eventPaid;
        if (message['paid_input'] === undefined || message['paid_input'] === '' ) {
            eventPaid = message['paid_default'];
        }
        else {
            eventPaid = message['paid_input'];
        }
        let eventArgs = {
            data: {
                account: socket.request.session.account_ref,
                permit: socket.request.session.permit,
                ref: message['ref_default'],
                language: eventLanguage,
                summary: eventSummary,
                details: eventDetails,
                client: eventClient,
                provider: eventProvider,
                expected: eventExpected,
                completed: eventCompleted,
                paid: eventPaid
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('posting event data: %s', JSON.stringify(eventArgs.data));
        event.post(eventArgs, function(postResult) {
            pino.info('event post result: %s', JSON.stringify(postResult));
        });  
    });  // end event save message response block 
    socket.on('item-save', function(message) {
        // complete new item process
        pino.info('item-save: %s', JSON.stringify(message)); 
        var itemName;
        if (message['name_input'] === undefined || message['name_input'] === '' ) {
            itemName = message['name_default'];
        }
        else {
            itemName = message['name_input'];
        }
        var itemFullName;
        if (message['full_name_input'] === undefined || message['full_name_input'] === '' ) {
            itemFullName = message['full_name_default'];
        }
        else {
            itemFullName = message['full_name_input'];
        }
        var itemLanguage;
        if (message['language_input'] === undefined || message['language_input'] === '' ) {
            itemLanguage = message['language_default'];
        }
        else {
            itemLanguage = message['language_input'];
        }
        var itemType;
        if (message['type_input'] === undefined || message['type_input'] === '' ) {
            itemType = message['type_default'];
        }
        else {
            itemType = message['type_input'];
        }
        var itemDescription;
        if (message['description_input'] === undefined || message['description_input'] === '' ) {
            itemDescription = message['description_default'];
        }
        else {
            itemDescription = message['description_input'];
        }
        var itemNotes;
        if (message['notes_input'] === undefined || message['notes_input'] === '' ) {
            itemNotes = message['notes_default'];
        }
        else {
            itemNotes = message['notes_input'];
        }
        let itemArgs = {
            data: {
                permit: socket.request.session.permit,
                account: socket.request.session.account_ref,
                ref: message['ref_default'],
                name: itemName,
                full_name: itemFullName,
                language: itemLanguage,
                type: itemType,
                description: itemDescription,
                notes: itemNotes
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('pino post item data %s', JSON.stringify(itemArgs.data));
        item.post(itemArgs);  
    });
    socket.on('issue-save', function(message) {
        // complete new item process
        pino.info('issue-save: %s', JSON.stringify(message)); 
        var issueLanguage;
        if (message['language_input'] === undefined || message['language_input'] === '' ) {
            issueLanguage = message['language_default'];
        }
        else {
            issueLanguage = message['language_input'];
        }
        var issueTopic;
        if (message['topic_input'] === undefined || message['topic_input'] === '' ) {
            issueTopic = message['topic_default'];
        }
        else {
            issueTopic = message['topic_input'];
        }
        var issueDescription;
        if (message['description_input'] === undefined || message['description_input'] === '' ) {
            issueDescription = message['description_default'];
        }
        else {
            issueDescription = message['description_input'];
        }
        var issueNotes;
        if (message['notes_input'] === undefined || message['notes_input'] === '' ) {
            issueNotes = message['notes_default'];
        }
        else {
            issueNotes = message['notes_input'];
        }
        var issueSubmitterName;
        if (message['submitter_name_input'] === undefined || message['submitter_name_input'] === '' ) {
            issueSubmitterName = message['submitter_name_default'];
        }
        else {
            issueSubmitterName = message['submitter_name_input'];
        }
        var issueOwnerName;
        if (message['owner_name_input'] === undefined || message['owner_name_input'] === '' ) {
            issueOwnerName = message['owner_name_default'];
        }
        else {
            issueOwnerName = message['owner_name_input'];
        }
        let issueArgs = {
            data: {
                permit: socket.request.session.permit,
                account_name: socket.request.session.account_name,
                ref: message['ref_default'],
                language: issueLanguage,
                topic: issueTopic,
                description: issueDescription,
                notes: issueNotes,
                submitter_name: issueSubmitterName,
                owner_name: issueOwnerName
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('pino post issue data %s', JSON.stringify(issueArgs.data));
        issue.post(issueArgs); 
    });
    // request save
    socket.on('request-save', function(message) {
        // complete new request process
        pino.info('request-save: %s', JSON.stringify(message)); 
        // lang, topic, descr, notes, submitter, owner
        var requestLanguage;
        if (message['language_input'] === undefined || message['language_input'] === '' ) {
            requestLanguage = message['language_default'];
        }
        else {
            requestLanguage = message['language_input'];
        }
        var requestSummary;
        if (message['summary_input'] === undefined || message['summary_input'] === '' ) {
            requestSummary = message['summary_default'];
        }
        else {
            requestSummary = message['summary_input'];
        }
        var requestDetails;
        if (message['details_input'] === undefined || message['details_input'] === '' ) {
            requestDetails = message['details_default'];
        }
        else {
            requestDetails = message['details_input'];
        }
        var requestNotes;
        if (message['notes_input'] === undefined || message['notes_input'] === '' ) {
            requestNotes = message['notes_default'];
        }
        else {
            requestNotes = message['notes_input'];
        }
        var requestSubmitterName;
        if (message['submitter_name_input'] === undefined || message['submitter_name_input'] === '' ) {
            requestSubmitterName = message['submitter_name_default'];
        }
        else {
            requestSubmitterName = message['submitter_name_input'];
        }
        var requestOwnerName;
        if (message['owner_name_input'] === undefined || message['owner_name_input'] === '' ) {
            requestOwnerName = message['owner_name_default'];
        }
        else {
            requestOwnerName = message['owner_name_input'];
        }
        let requestArgs = {
            data: {
                permit: socket.request.session.permit,
                account_name: socket.request.session.account_name,
                ref: message['ref_default'],
                language: requestLanguage,
                summary: requestSummary,
                details: requestDetails,
                notes: requestNotes,
                submitter_name: requestSubmitterName,
                owner_name: requestOwnerName
            },
            headers: {'Content-Type': 'application/json'}
        };
        pino.info('pino post request data %s', JSON.stringify(requestArgs.data));
        request.post(requestArgs); 
    });

});  // end socket response block

server.listen(config.port, function() {
    pino.info('listen to port %d', config.port);
});
