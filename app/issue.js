// issue.js -- issue related functionality
// !!! initial sketch only, not used

// internal modules
let config = require('./config');
let local = require('./localize');
let service = require('./service');
// external modules
let pino = require('pino')();
let clientSource = require('node-rest-client').Client;
// object for exporting routines
var issue = {};

function issueCreate(language, account_name, permit, result) {
    pino.info('create issue');
    // some fields simply start empty but empty ref indicates new issue creation
    issueInspector(language, result, '', language, '', '', '', '', account_name, '');
}

function issueSelector(viewLanguage, result, count, ref, language, topic, description, status) {
    // first row are labels for columns being a subset of most relevant fields
    let languageText = local.text(viewLanguage, 'language');
    let topicText = local.text(viewLanguage, 'topic');
    let descriptionText = local.text(viewLanguage, 'description');
    let statusText = local.text(viewLanguage, 'status');
    // buttons
    let inspectText = local.text(viewLanguage, 'inspect');
    let deleteText = local.text(viewLanguage, 'delete');
    let closeText = local.text(viewLanguage, 'close');
    result.render('issues', {
        language_label: languageText,
        topic_label: topicText,
        description_label: descriptionText,
        status_label: statusText,
        inspect_label: inspectText,
        delete_label: deleteText,
        refs: ref,
        languages: language,
        topics: topic,
        descriptions: description,
        statuses: status,
        close: closeText
    });
}

function issueSelect(viewLanguage, account_name, permit, result) {
    pino.info('issue select');
    let fetchIssueArgs = {
        data: {
            language: viewLanguage,
            account_name: account_name,
            permit: permit
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('fetch issue args: %s', JSON.stringify(fetchIssueArgs));
    service.get(config.proTender + '/issue', fetchIssueArgs, function(fetchResult) {
        pino.info('issue fetch result count: %s', fetchResult.count);
        if (fetchResult.count === 0) {
            return;
        }
        pino.info('issue fetch result data: %s', JSON.stringify(fetchResult.data));
        var ref = {};
        var language = {};
        var topic = {};
        var description = {};
        var status = {};
        for (var issueIndex = 0; issueIndex < fetchResult.count; issueIndex++) {
            pino.info('issue %d: ref=%s, name=%s', issueIndex,
                fetchResult.data[issueIndex].ref,
                fetchResult.data[issueIndex].name);
            ref[issueIndex] = fetchResult.data[issueIndex].ref;
            language[issueIndex] = fetchResult.data[issueIndex].language;
            topic[issueIndex] = fetchResult.data[issueIndex].topic;
            description[issueIndex] = fetchResult.data[issueIndex].description;
            status[issueIndex] = fetchResult.data[issueIndex].status;
        }
        issueSelector(viewLanguage, result, fetchResult.count, 
            ref, language, topic, description, status);
    });
}

function issueInspector(viewLanguage, result, ref, issueLanguage, topic, description, status, notes, submitterName, ownerName) {
    pino.info('issue inspector: vlang = %s, ref = %s, topic = %s',
        viewLanguage, ref, topic);
    // first generate common text
    let sectionText = local.text(viewLanguage, 'issue-section');
    let languageText = local.text(viewLanguage, 'language');
    let topicText = local.text(viewLanguage, 'topic');
    let descriptionText = local.text(viewLanguage, 'description');
    let statusText = local.text(viewLanguage, 'status');
    let notesText = local.text(viewLanguage, 'notes');
    let submitterNameText = local.text(viewLanguage, 'submitter-name');
    let ownerNameText = local.text(viewLanguage, 'owner-name');
    // button labels
    let saveText = local.text(viewLanguage, 'save');
    let closeText = local.text(viewLanguage, 'close');
    // render inspector
    result.render('issue', {
        issue_section: sectionText,
        ref: ref,
        language: languageText,
        language_value: issueLanguage,
        topic: topicText,
        topic_value: topic,
        description: descriptionText,
        description_value: description,
        status: statusText,
        status_value: status,
        notes: notesText,
        notes_value: notes,
        submitter_name: submitterNameText,
        submitter_name_value: submitterName,
        owner_name: ownerNameText,
        owner_name_value: ownerName,
        save: saveText,
        close: closeText
    });
}

function issueInspect(language, account_name, permit, ref, result) {
    pino.info('issue inspect ref=%s', ref);
    let issueFetchArgs = {
        data: {
            account_name: account_name,
            permit: permit,
            ref: ref
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for issue fetch: %s', JSON.stringify(issueFetchArgs));
    service.get(config.proTender + '/issue', issueFetchArgs, function(data) {
        pino.info('issue fetch result: %s', data);
        issueInspector(language, result, data.data[0].ref,
            data.data[0].language, data.data[0].topic, data.data[0].description,
            data.data[0].status, data.data[0].notes, 
            data.data[0].submitter_name, data.data[0].owner_name
        );
    });
}

function issuePost(issueArgs) {
    service.post(config.proTender + '/issue', issueArgs, function(data) {
        pino.info('issue post returned: %s', JSON.stringify(data));
    });
}

function issueConfirm(language, issueRef, result) {
    pino.info('issue delete confirm ref=%s', issueRef);
    let confirmText = local.text(language, 'issue-delete-confirm');
    let closeText = local.text(language, 'close');
    let deleteText = local.text(language, 'delete');
    result.render('confirm-delete', {
        confirm_text: confirmText,
        close_text: closeText,
        delete_text: deleteText,
        type: 'issue',
        ref: issueRef
    });
}

function issueDelete(account_name, permit, issueRef) {
    let deleteArgs = {
        data: {
            account_name: account_name,
            permit: permit,
            ref: issueRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('delete issue, data: %s', JSON.stringify(deleteArgs.data));
    service.post(config.proTender + '/issue/delete', deleteArgs, function(data) {
        pino.info('issue delete returns: %s', JSON.stringify(data));
    });
}

issue.create = issueCreate;
issue.select = issueSelect;
issue.inspect = issueInspect;
issue.post = issuePost;
issue.confirm = issueConfirm;
issue.delete = issueDelete;
module.exports = issue;
