// item.js -- item related functionality
// !!! initial sketch only, not used

// internal modules
let config = require('./config');
let local = require('./localize');
let service = require('./service');
// external modules
let pino = require('pino')();
let clientSource = require('node-rest-client').Client;
// object for exporting routines
var item = {};

function itemCreate(language, account_name, permit, result) {
    pino.info('create item');
    // some fields simply start empty but empty ref indicates new item creation
    itemInspector(language, result, '', '', '', language, '', '');
}

function itemSelector(viewLanguage, result, count, ref, name, full_name, language, type) {
    // first row are labels for columns being a subset of most relevant fields
    let nameText = local.text(viewLanguage, 'name');
    let fullNameText = local.text(viewLanguage, 'full-name');
    let languageText = local.text(viewLanguage, 'language');
    // buttons
    let inspectText = local.text(viewLanguage, 'inspect');
    let deleteText = local.text(viewLanguage, 'delete');
    let closeText = local.text(viewLanguage, 'close');
    result.render('items', {
        name_label: nameText,
        full_name_label: fullNameText,
        language_label: languageText,
        inspect_label: inspectText,
        delete_label: deleteText,
        refs: ref,
        names: name,
        full_names: full_name,
        languages: language,
        types: type,
        close: closeText
    });
}

function itemSelect(viewLanguage, account_name, permit, result) {
    pino.info('item select');
    let fetchItemArgs = {
        data: {
            language: viewLanguage,
            account_name: account_name,
            permit: permit
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('fetch item args: %s', JSON.stringify(fetchItemArgs));
    service.get(config.proTender + '/item', fetchItemArgs, function(fetchResult) {
        pino.info('item fetch result count: %s', fetchResult.count);
        if (fetchResult.count === 0) {
            return;
        }
        pino.info('item fetch result data: %s', JSON.stringify(fetchResult.data));
        var ref = {};
        var name = {};
        var full_name = {};
        var language = {};
        var type = {};
        for (var itemIndex = 0; itemIndex < fetchResult.count; itemIndex++) {
            pino.info('item %d: ref=%s, name=%s', itemIndex,
                fetchResult.data[itemIndex].ref,
                fetchResult.data[itemIndex].name);
            ref[itemIndex] = fetchResult.data[itemIndex].ref;
            name[itemIndex] = fetchResult.data[itemIndex].name;
            full_name[itemIndex] = fetchResult.data[itemIndex].full_name;
            language[itemIndex] = fetchResult.data[itemIndex].language;
            type[itemIndex] = fetchResult.data[itemIndex].type;
        }
        itemSelector(viewLanguage, result, fetchResult.count, 
            ref, name, full_name, language, type);
    });
}

function itemInspector(viewLanguage, result, ref, name, fullName, itemLanguage, type, description) {
    pino.info('item inspector: vlang = %s, ref = %s, name = %s',
        viewLanguage, ref, name);
    // first generate common text
    let sectionText = local.text(viewLanguage, 'item-section');
    let nameText = local.text(viewLanguage, 'name');
    let fullNameText = local.text(viewLanguage, 'full-name');
    let languageText = local.text(viewLanguage, 'language');
    let typeText = local.text(viewLanguage, 'type');
    let descriptionText = local.text(viewLanguage, 'description');
    // other fields ...
    let saveText = local.text(viewLanguage, 'save');
    let closeText = local.text(viewLanguage, 'close');
    // render item inspector
    result.render('item', {
        item_section: sectionText,
        ref: ref,
        name: nameText,
        name_value: name,
        full_name: fullNameText,
        full_name_value: fullName,
        language: languageText,
        language_value: itemLanguage,
        type: typeText,
        type_value: type,
        description: descriptionText,
        description_value: description,
        save: saveText,
        close: closeText
    });
}

function itemInspect(language, account_name, permit, ref, result) {
    pino.info('item inspect ref=%s', ref);
    let itemFetchArgs = {
        data: {
            account_name: account_name,
            permit: permit,
            ref: ref
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for item fetch: %s', JSON.stringify(itemFetchArgs));
    service.get(config.proTender + '/item', itemFetchArgs, function(data) {
        pino.info('item fetch result: %s', data);
        itemInspector(language, result, data.data[0].ref,
            data.data[0].name, data.data[0].full_name, data.data[0].language,
            data.data[0].type, data.data[0].description);
    });
}

function itemPost(itemArgs) {
    service.post(config.proTender + '/item', itemArgs, function(data) {
        pino.info('item post returned: %s', JSON.stringify(data));
    });
}

function itemConfirm(language, itemRef, result) {
    pino.info('item delete confirm ref=%s', itemRef);
    let confirmText = local.text(language, 'item-delete-confirm');
    let closeText = local.text(language, 'close');
    let deleteText = local.text(language, 'delete');
    result.render('confirm-delete', {
        confirm_text: confirmText,
        close_text: closeText,
        delete_text: deleteText,
        type: 'item',
        ref: itemRef
    });
}

function itemDelete(account_name, permit, itemRef) {
    let deleteArgs = {
        data: {
            account_name: account_name,
            permit: permit,
            ref: itemRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('delete item, data: %s', JSON.stringify(deleteArgs.data));
    service.post(config.proTender + '/item/delete', deleteArgs, function(data) {
        pino.info('item delete returns: %s', JSON.stringify(data));
    });
}

item.create = itemCreate;
item.select = itemSelect;
item.inspect = itemInspect;
item.post = itemPost;
item.confirm = itemConfirm;
item.delete = itemDelete;
module.exports = item;
