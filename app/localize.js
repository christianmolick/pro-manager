// localize.js
// all interfaces and mechanisms
// for selecting language, retaining and recalling selection,
// and generating translated messages by language
// required modules
let pino = require('pino')();
let fs = require('fs');
let fb = require('@fluent/bundle');
// object for state and exported routines
var local = {}; 
local['supported'] = ['en', 'es', 'pt'];  // should be part of initialization?

// load fluent and translations for languages
// generate bundle, read messages, add as resources while saving all errors
function localInitialize() {
    local['bundle'] = {};
    local['errors'] = {};
    for (var languageIndex = 0; languageIndex < local.supported.length; languageIndex++) {
        local['bundle'][local.supported[languageIndex]] = new fb.FluentBundle(local.supported[languageIndex]);
        //languageBundle[supportedLanguages[languageIndex]] = new fb.FluentBundle(supportedLanguages[languageIndex]);
        local['messages'] = fs.readFileSync('locales/' + local.supported[languageIndex] + '/messages.ftl', 'utf8');
        // languageMessages = fs.readFileSync('locales/' + supportedLanguages[languageIndex] + '/messages.ftl', 'utf8');
        pino.info('language: %s, total message length: %d', local.supported[languageIndex], local['messages'].length);
        local['errors'][local.supported[languageIndex]] = local['bundle'][local.supported[languageIndex]].addResource(new fb.FluentResource(local['messages']));
    }
}

// language options: renders a language panel populated with all options
function localOptions(language, res) {
    pino.info('generating local options panel, counting supported languages');
    var supportedLanguageCount = local.supported.length;
    pino.info('supported language count: %s', supportedLanguageCount);
    pino.info('defining and clearing language options');
    var languageOptions = {};
    var languageMessage;
    // construct a list of names for supported language options
    for (var languageIndex = 0; languageIndex < supportedLanguageCount; languageIndex++) {
        languageMessage = local.bundle[local.supported[languageIndex]].getMessage(local.supported[languageIndex]);
        languageOptions[local.supported[languageIndex]] = local.bundle[local.supported[languageIndex]].formatPattern(languageMessage.value);
        pino.info('language option: %s = %s', local.supported[languageIndex].toString(), languageOptions[local.supported[languageIndex]]);
    }
    // text for title and close button
    let languageOptionsMessage = local.bundle[language].getMessage('language-options-label');
    let languageOptionsText = local.bundle[language].formatPattern(languageOptionsMessage.value);
    pino.info('language options text: %s', languageOptionsText);
    let closePanelMessage = local.bundle[language].getMessage('close');
    let closePanelText = local.bundle[language].formatPattern(closePanelMessage.value);
    pino.info('close panel text: %s', closePanelText);
    res.render('languages', {
        'language_options_text': languageOptionsText,
        'language_options': languageOptions,
        close: closePanelText
    });
}  // end language options panel block

// localText returns text for a specific translated message using given language code
function localText(language, messageCode) {
    //pino.info('localizing text for message code %s', messageCode);
    let message = local.bundle[language].getMessage(messageCode);
    let messageText = local.bundle[language].formatPattern(message.value);
    //pino.info('result is %s', messageCode, messageText);
    return(messageText);
}

// localStrings returns a hash of language code and message strings
// for supported languages
function localStrings() {
    pino.info('generating locale codes and strings, counting supported languages');
    var supportedLanguageCount = local.supported.length;
    pino.info('supported language count: %s', supportedLanguageCount);
    var languageOptions = {};
    var languageMessage;
    // construct a list of names for supported language options
    for (var languageIndex = 0; languageIndex < supportedLanguageCount; languageIndex++) {
        languageMessage = local.bundle[local.supported[languageIndex]].getMessage(local.supported[languageIndex]);
        languageOptions[local.supported[languageIndex]] = local.bundle[local.supported[languageIndex]].formatPattern(languageMessage.value);
        pino.info('language string: %s = %s', local.supported[languageIndex].toString(), languageOptions[local.supported[languageIndex]]);
    }
    return languageOptions;
}

// old code retained only for reference
function oldLocalStrings() {
    var languageStrings = [];
    // construct a list of names for supported language options
    for (var languageIndex = 0; languageIndex < supportedLanguageCount; languageIndex++) {
        // language code
        languageStrings[languageIndex * 2] = local.supported[languageIndex];
        // translated language reference
        // note language messages are translated based on the language they represent
        // and not the currently selected interface language
        languageMessage = local.bundle[local.supported[languageIndex]].getMessage(local.supported[languageIndex]);
    }
    return languageStrings;
}

// compose and export module elements
local.init = localInitialize;
local.options = localOptions;
local.text = localText;
local.strings = localStrings;
module.exports = local;
