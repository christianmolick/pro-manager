// person.js -- person related functionality
// internal modules
let config = require('./config');
let local = require('./localize');
let service = require('./service');
// external modules
let pino = require('pino')();
let clientSource = require('node-rest-client').Client;
//    ref UUID,
//    container UUID,
//    name TEXT,
//    full_name TEXT,
//    language TEXT,    
//    account UUID,
//    place UUID,
//    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    profile TEXT,
//    preferences TEXT,
//    provides TEXT
// !!! container, place, account all need to be evaluated
//     account_name to account is particularly needed

// personCreate raises an inspector with default data
// !!! at this time no database record is made and no structure holds all data
function personCreate(language, accountRef, permitRef, result) {
    pino.info('create person');
    // compose person record for inspection
    // send composition to service
    let newPersonData = {
        data: {
            account: accountRef,
            permit: permitRef,
            language: language
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for person create: %s', JSON.stringify(newPersonData));
    personPost(newPersonData, function(postResult) {
        newPersonData.data.ref = postResult.ref;
        personInspector(language, accountRef, permitRef, newPersonData.data, result);
    });
}

// personOperator shows person records and enables inspect and delete
// !!! fails badly with zero records case
function personOperator(viewLanguage, result, count, personRef, name, full_name, language) {
    // first row has column labels
    // columns are a subset of most interesting/relevant fields
    let nameText = local.text(viewLanguage, 'name');
    let fullNameText = local.text(viewLanguage, 'full-name');
    let languageText = local.text(viewLanguage, 'language');
    // buttons
    let inspectText = local.text(viewLanguage, 'inspect');
    let deleteText = local.text(viewLanguage, 'delete');
    let closeText = local.text(viewLanguage, 'close');
    result.render('persons', {
        refs: personRef,
        language_label: languageText,
        languages: language,
        name_label: nameText,
        names: name,
        full_name_label: fullNameText,
        full_names: full_name,
        inspect_label: inspectText,
        delete_label: deleteText,
        close: closeText
    });
}

// personSelector enables selection instead of inspect/delete (is a chooser)
// and is used in the context of an inspector
// selectionRef is the tracking record and ref is an array of person refs for selection
function personSelector(viewLanguage, result, count, selectionRef, ref, name, full_name, language) {
    // first row has column labels
    // columns are a subset of most interesting/relevant fields
    let nameText = local.text(viewLanguage, 'name');
    let fullNameText = local.text(viewLanguage, 'full-name');
    let languageText = local.text(viewLanguage, 'language');
    // buttons
    let selectText = local.text(viewLanguage, 'select');
    let closeText = local.text(viewLanguage, 'close');
    // !!! in usage the selection ref and array of refs are confused?
    for (i = 0; i < count; i++) {
        pino.info('personer #%d: ref = %s', i, ref[i]);
    }
    result.render('personer', {
        selection_ref: selectionRef,
        refs: ref,
        name_label: nameText,
        names: name,
        full_name_label: fullNameText,
        full_names: full_name,
        language_label: languageText,
        languages: language,
        select_label: selectText,
        close: closeText
    });
}

// personSelect takes an account and permit
// finds all the person records for that account
// and raises a panel that lists them and enables inspect and delete actions
// !!! REFACTOR IN PROGRESS HERE
// function placeSelect(viewLanguage, accountRef, permitRef, placeRef, selectionRef, result)
//function placeSelect(viewLanguage, accountRef, permitRef, placeRef, selectionRef, result)
function personSelect(viewLanguage, accountRef, permitRef, selectionRef, result) {
    pino.info('person select lang = %s, account = %s, permit = %s, selection = %s', viewLanguage, accountRef, permitRef, selectionRef);
    // note: to be here permit check must have been completed
    // fetch data for persons based on account_name parameter
    let fetchPersonsArgs = {
        data: {
            language: viewLanguage,
            account: accountRef,
            permit: permitRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('fetch person account: %s, permit: %s', accountRef, permitRef);
    service.get(config.proTender + '/person', fetchPersonsArgs, function(fetchResult) {
        pino.info('persons fetch result count %d', fetchResult.count);  
        if (fetchResult.count === 0) {
            return;
        }
        pino.info('person fetch result: %s', JSON.stringify(fetchResult.data));
        // fill and launch selector
        var ref={};
        var name={};
        var full_name={};
        var language={};
        for (var personIndex = 0; personIndex < fetchResult.count; personIndex++) {
            pino.info('person %d: ref=%s name=%s full_name=%s language=%s', personIndex,
                fetchResult.data[personIndex].ref,
                fetchResult.data[personIndex].name,
                fetchResult.data[personIndex].full_name,
                fetchResult.data[personIndex].language
            );
            ref[personIndex] = fetchResult.data[personIndex].ref,
            name[personIndex] = fetchResult.data[personIndex].name,
            full_name[personIndex] = fetchResult.data[personIndex].full_name, 
            language[personIndex] = fetchResult.data[personIndex].language
        }
        // language for rendering, result for http request, count, persons array
        if (selectionRef === undefined || selectionRef === null || selectionRef === '') {
            personOperator(viewLanguage, result, fetchResult.count, ref, name, full_name, language);
        }
        else {
            personSelector(viewLanguage, result, fetchResult.count, selectionRef, ref, name, full_name, language);
        }
    });  // end fetch person data block
}

// personInspector raises an inspector panel given person data
function personInspector(viewLanguage, accountRef, permitRef, personRecord, result) {
    // generate all common text
    let sectionText = local.text(viewLanguage, 'person-section');
    let languageText = local.text(viewLanguage, 'language');
    let nameText = local.text(viewLanguage, 'name');
    let fullNameText = local.text(viewLanguage, 'full-name');
    let profileText = local.text(viewLanguage, 'profile');
    let preferencesText = local.text(viewLanguage, 'preferences');
    let providesText = local.text(viewLanguage, 'provides');
    let containerText = local.text(viewLanguage, 'container');
    let selectText = local.text(viewLanguage, 'select');
    let saveText = local.text(viewLanguage, 'person-save');
    let closeText = local.text(viewLanguage, 'close');
    personSummary(accountRef, permitRef, personRecord.container, function(containerValue) {
        // INDENTINGE!
    // additional business logic goes here
    // make new place data or fetch from table
    // person reference may be name, uuid, other--empty means new
    result.render('person', {
        ref: personRecord.ref,
        person_section: sectionText,
        language: languageText,
        language_value: personRecord.language, 
        name: nameText,
        name_value: personRecord.name, 
        full_name: fullNameText,
        full_name_value: personRecord.full_name,
        profile: profileText,
        profile_value: personRecord.profile,
        preferences: preferencesText,
        preferences_value: personRecord.preferences, 
        provides: providesText,
        provides_value: personRecord.provides,
        container: containerText,
        container_value: containerValue,
        select_label: selectText,
        save: saveText,
        close: closeText
    });
    
    });  // end person summary block
}  // end person inspector block

function personInspect(language, accountRef, permitRef, personRef, result) {
    pino.info('person inspect person ref=%s', personRef);
    // fetch data for person selected for inspection
    let personFetchArgs = {
        data: {
            account: accountRef,
            permit: permitRef,
            ref: personRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for person fetch: %s', JSON.stringify(personFetchArgs));
    service.get(config.proTender + '/person', personFetchArgs, function(data) {
        pino.info('person fetch data: %s', JSON.stringify(data.data));
        // display inspector with fetched person data
        personInspector(language, accountRef, permitRef, data.data[0], result);
    });
}

// person post is used for updates to an existing record
// !!! must convert account_name argument to account uuid ref
function personPost(personArgs, callback) {
    service.post(config.proTender + '/person', personArgs, function(data) {
        pino.info('post person returns: %s', JSON.stringify(data));
        callback(data);
    });
    //service.get(config.accountTender + '/account/inspect', personArgs, function(accountFetchData) {
    //    personArgs.account = accountFetchData.ref;
    //    service.post(config.proTender + '/person', personArgs, function(personPostData) {
    //        pino.info('post person returns: %s', JSON.stringify(personPostData));
    //    });
    //});
}

// personSummary returns a short string summarizing person data
// !!! refactor to use summary generator from 
function personSummary(accountRef, permitRef, personRef, callback) {
    pino.info('person summary for ref=%s', personRef);
    if (personRef === undefined || personRef === null|| personRef === '') {
        callback('');
        return;
    }
    // fetch data for place selected for inspection
    let personFetchArgs = {
        data: {
            account: accountRef,
            permit: permitRef,
            ref: personRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for person fetch: %s', JSON.stringify(personFetchArgs));
    service.get(config.proTender + '/person', personFetchArgs, function(data) {
        pino.info('person fetch data: %s', JSON.stringify(data));
        var summary = data.data[0].name;
        if (data.data[0].full_name != undefined && data.data[0].full_name != '') {
            summary += ', ' + data.data[0].full_name;
        }
        if (data.data[0].type != undefined && data.data[0].type != '') {
            summary += ', ' + data.data[0].type;
        }
        pino.info('person summary: %s', summary);
        // !!! returns empty string if problems with lookup, should be "empty" instead?
        callback(summary);
    });
}

// person confirm is the first of a two step process
// !!! account ref not needed because picked up later?
function personConfirm(language, accountRef, personRef, result) {
    // person delete button is on person selector, this is the confirmation panel
    let confirmText = local.text(language, 'person-delete-confirm');
    let closeText = local.text(language, 'close');
    let deleteText = local.text(language, 'delete');
    // additional business logic goes here
    // make new person data or fetch from table
    // person reference may be name, uuid, other--empty means new
    result.render('confirm-delete', {
        confirm_text: confirmText,
        close_text: closeText,
        delete_text: deleteText,
        type: 'person',
        ref: personRef
    });    
}

function personDelete(account_ref, permit, personRef) {
    let deleteArgs = {
        data: {
            account: account_ref,
            permit: permit,
            ref: personRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('delete person, data: %s', JSON.stringify(deleteArgs.data));
    service.post(config.proTender + '/person/delete', deleteArgs, function(data) {
        pino.info('person delete returns: %s', JSON.stringify(data));
    });
}

// object for exporting routines
var person = {};
person.create = personCreate;
person.operator = personOperator;  // internal only?
person.selector = personSelector;  // internal only?
person.select = personSelect;
person.inspector = personInspector;  // internal only?
person.inspect = personInspect;
person.post = personPost;
person.summary = personSummary;
person.confirm = personConfirm;
person.delete = personDelete;
module.exports = person;

