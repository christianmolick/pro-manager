// place.js -- place related functionality
// internal modules
let config = require('./config');
let local = require('./localize');
let service = require('./service');
// external modules
let pino = require('pino')();
let clientSource = require('node-rest-client').Client;

// placeCreate makes a basic place record with language only
// and then raises and inspector panel with that data
// not clear if best to create default accounts and then edit
// or to wait for first edit to actually save database records
// first sketch raises a panel with string data only and null ref
function placeCreate(language, accountRef, permitRef, result) {
    pino.info('create place: lang = %s, account = %s, permit = %s', language, accountRef, permitRef);
    // send default parameters to tender
    let newPlaceData = {
        data: {
            account: accountRef,
            permit: permitRef,
            language: language
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for place create: %s', JSON.stringify(newPlaceData));
    placePost(newPlaceData, function(postResult) {
        newPlaceData.data.ref = postResult.ref;
        placeInspector(language, accountRef, permitRef, newPlaceData.data, result);
    });
}

// placeSelector takes arrays of place data
// and shows displays them in rows to enable selection
// first sketch shows only language, name, and full name
// with the full data being available in the inspector
// !!! fails badly if zero records
function placeOperator(viewLanguage, result, count, placeRef, name, full_name, language, type) {
    // first row has column labels
    // columns are a subset of most interesting/relevant fields
    let nameText = local.text(viewLanguage, 'name');
    let fullNameText = local.text(viewLanguage, 'full-name');
    let languageText = local.text(viewLanguage, 'language');
    let typeText = local.text(viewLanguage, 'type');
    // action buttons
    let inspectText = local.text(viewLanguage, 'inspect');
    let deleteText = local.text(viewLanguage, 'delete');
    let closeText = local.text(viewLanguage, 'close');
    result.render('places', {
        refs: placeRef,
        language_label: languageText,
        languages: language,
        name_label: nameText,
        names: name,
        full_name_label: fullNameText,
        full_names: full_name,
        inspect_label: inspectText,
        delete_label: deleteText,
        types: type,
        close: closeText
    });
}

// placeChooser is basically a selector
// but instead of enabling inspect and delete
// it only allows selection
function placeSelector(viewLanguage, result, count, selectionRef, ref, name, full_name, language, type) {
    // first row has column labels
    // columns are a subset of most interesting/relevant fields
    let nameText = local.text(viewLanguage, 'name');
    let fullNameText = local.text(viewLanguage, 'full-name');
    let languageText = local.text(viewLanguage, 'language');
    let typeText = local.text(viewLanguage, 'type');
    // action buttons
    let selectText = local.text(viewLanguage, 'select');
    let closeText = local.text(viewLanguage, 'close');
    pino.info('raising placer panel: selref = %s, names = %s', selectionRef, JSON.stringify(name));
    result.render('placer', {
        selection_ref: selectionRef,
        refs: ref,
        language_label: languageText,
        languages: language,
        name_label: nameText,
        names: name,
        full_name_label: fullNameText,
        full_names: full_name,
        select_label: selectText,
        types: type,
        close: closeText
    });
}

// placeSelect takes an account and permit
// finds all the viewable place records for that account
// and then raises a place selector panel with those records
// returned data is an easily visible subset only:
// language, name, full name, type -- timestamps and notes not included
// !!! language may be passed along or ignored in this first sketch
// !!! fails badly if zero records
// !!! not clear if both placeRef and selectionRef needed ... maybe only selectionRef?
function placeSelect(viewLanguage, accountRef, permitRef, placeRef, selectionRef, result) {
    pino.info('place select lang = %s, account = %s, permit = %s, place = %s, selection = %s',
        viewLanguage, accountRef, permitRef, placeRef, selectionRef);
    // note: to be here permit check must have been completed
    // fetch data for places based on account_name parameter
    let fetchPlacesArgs = {
        data: {
            language: viewLanguage,
            account: accountRef,
            permit: permitRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('fetch viewable place records for account: %s with permit: %s', accountRef, permitRef);
    service.get(config.proTender + '/place', fetchPlacesArgs, function(fetchResult) {
        pino.info('places fetch result count %d', fetchResult.count);  
        if (fetchResult.count === 0) {
            return;
        }
        console.log('place fetch result:', JSON.stringify(fetchResult.data));
        // fill and launch selector
        var ref={};
        var name={};
        var full_name={};
        var language={};
        var type={};
        for (var placeIndex = 0; placeIndex < fetchResult.count; placeIndex++) {
            pino.info('place %d: ref=%s language=%sname=%s full_name=%s  type=%s', placeIndex,
                fetchResult.data[placeIndex].ref,
                fetchResult.data[placeIndex].language,
                fetchResult.data[placeIndex].name,
                fetchResult.data[placeIndex].full_name,
                fetchResult.data[placeIndex].type
            );
            ref[placeIndex] = fetchResult.data[placeIndex].ref,
            name[placeIndex] = fetchResult.data[placeIndex].name,
            full_name[placeIndex] = fetchResult.data[placeIndex].full_name, 
            language[placeIndex] = fetchResult.data[placeIndex].language, 
            type[placeIndex] = fetchResult.data[placeIndex].type
        }
        // without selection ref use full operator
        // but with specific selection use selector
        if (selectionRef === undefined || selectionRef === null || selectionRef === '') {
            placeOperator(viewLanguage, result, fetchResult.count, ref, name, full_name, language, type);
        }
        else {
            placeSelector(viewLanguage, result, fetchResult.count, selectionRef, ref, name, full_name, language, type);
        }
    });
}

// placeInspector raises a panel showsing place data and enabling changes
// !!! note currently missing are all of the created, viewed, modified time stamps
// from create: placeInspector(language, accountRef, permitRef, newPlaceData.data, result);
function placeInspector(language, accountRef, permitRef, placeRecord, result) {
    pino.info('place inspector: vlang = %s, ref = %s, name = %s, fullName = %s, lang = %s, type = %s, descr = %s', language, placeRecord.ref, placeRecord.name, placeRecord.full_name, placeRecord.language, placeRecord.type, placeRecord.description);
    // generate localized text strings
    let sectionText = local.text(language, 'place-section');
    let languageText = local.text(language, 'language');
    let nameText = local.text(language, 'name');
    let fullNameText = local.text(language, 'full-name');
    let typeText = local.text(language, 'type');
    let descriptionText = local.text(language, 'description');
    let containerText = local.text(language, 'container');
    //let containerValue = placeRecord.name + ', ' + placeRecord.full_name;
    //let containerValue = placeSummary(accountRef, permitRef, placeRecord.container);
    //function placeSummary(accountRef, permitRef, placeRef, callback) {
    placeSummary(accountRef, permitRef, placeRecord.container, function(containerValue) {
        
        // INDENTINGE!!!
    let selectText = local.text(language, 'select');
    let saveText = local.text(language, 'save');
    let closeText = local.text(language, 'close');
    // additional business logic goes here
    // make new place data or fetch from table
    // place reference may be name, uuid, other--empty means new
    result.render('place', {
        ref: placeRecord.ref,
        place_section: sectionText,
        language: languageText,
        language_value: placeRecord.language, 
        name: nameText,
        name_value: placeRecord.name, 
        full_name: fullNameText,
        full_name_value: placeRecord.full_name,
        type: typeText,
        type_value: placeRecord.type, 
        description: descriptionText,
        description_value: placeRecord.description,
        container: containerText,
        container_value: containerValue,
        select: selectText,
        save: saveText,
        close: closeText
    });
        });  // end fetch summary block
}  // end place inspector block

// placeInspect gets place data and uses it with a place inspector
function placeInspect(language, accountRef, permitRef, placeRef, result) {
    pino.info('place inspect ref=%s', placeRef);
    // fetch data for place selected for inspection
    let placeFetchArgs = {
        data: {
            account: accountRef,
            permit: permitRef,
            ref: placeRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for place fetch: %s', JSON.stringify(placeFetchArgs));
    service.get(config.proTender + '/place', placeFetchArgs, function(data) {
        pino.info('place fetch data: %s', JSON.stringify(data.data));
        // display inspector with fetched data
        placeInspector(language, accountRef, permitRef, data.data[0], result);
    });
}

// placePost is used for updates to an existing record
function placePost(placeArgs, callback) {
    service.post(config.proTender + '/place', placeArgs, function(data) {
        pino.info('post place returns: %s', JSON.stringify(data));
        callback(data);
    });
}

// placeSummary returns a short string summarizing place data
// first sketch uses name, full name, and type
function placeSummary(accountRef, permitRef, placeRef, callback) {
    pino.info('place summary for place ref=%s', placeRef);
    if (placeRef === undefined || placeRef === null|| placeRef === '') {
        callback('');
        return;
    }
    // fetch data for place selected for inspection
    let placeFetchArgs = {
        data: {
            account: accountRef,
            permit: permitRef,
            ref: placeRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for place fetch: %s', JSON.stringify(placeFetchArgs));
    service.get(config.proTender + '/place', placeFetchArgs, function(data) {
        pino.info('place fetch data: %s', JSON.stringify(data));
        var summary = data.data[0].name;
        if (data.data[0].full_name != undefined && data.data[0].full_name != '') {
            summary += ', ' + data.data[0].full_name;
        }
        if (data.data[0].type != undefined && data.data[0].type != '') {
            summary += ', ' + data.data[0].type;
        }
        pino.info('place summary: %s', summary);
        // !!! returns empty string if problems with lookup, should be "empty" instead?
        callback(summary);
    });
}

// placeConfirm makes a panel that has option to move forward with deletion
// confirm action currently only used for deletion
function placeConfirm(language, placeRef, result) {
    // place delete button is on place selector, this is the confirmation panel
    let confirmText = local.text(language, 'place-delete-confirm');
    let closeText = local.text(language, 'close');
    let deleteText = local.text(language, 'delete');
    // additional business logic goes here
    // make new place data or fetch from table
    // place reference may be name, uuid, other--empty means new
    result.render('confirm-delete', {
        confirm_text: confirmText,
        close_text: closeText,
        delete_text: deleteText,
        type: 'place',
        ref: placeRef
    });    
}

// place delete simply removes the place record
function placeDelete(account, permit, placeRef) {
    let deleteArgs = {
        data: {
            account: account,
            permit: permit,
            ref: placeRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('delete place, data: %s', JSON.stringify(deleteArgs.data));
    service.post(config.proTender + '/place/delete', deleteArgs, function(data) {
        pino.info('place delete returns: %s', JSON.stringify(data));
    });
}

// object for exporting routines
var place = {};
place.create = placeCreate;
place.operator = placeOperator;
place.selector = placeSelector;
place.select = placeSelect;
place.inspector = placeInspector;
place.inspect = placeInspect;
place.post = placePost;
place.summary = placeSummary;
place.confirm = placeConfirm;
place.delete = placeDelete;
module.exports = place;

