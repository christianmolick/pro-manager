// request.js -- request related functionality
// !!! initial sketch only, not used

// internal modules
let config = require('./config');
let local = require('./localize');
let service = require('./service');
// external modules
let pino = require('pino')();
let clientSource = require('node-rest-client').Client;
// object for exporting routines
var request = {};

function requestCreate(language, account_name, permit, result) {
    pino.info('create request');
    // some fields simply start empty but empty ref indicates new request creation
    requestInspector(language, result, '', language, '', '', '', '', account_name, '');
}

// inspection: ref, lang, summary, details, status, submitter, provider, expected
// selection: summary, status, submitter, provider, expected
function requestSelector(viewLanguage, result, count, ref, language, summary, status, submiter, provider, expected) {
    // first row are labels for columns being a subset of most relevant fields
    let languageText = local.text(viewLanguage, 'language');
    let summaryText = local.text(viewLanguage, 'summary');
    let statusText = local.text(viewLanguage, 'status');
    let submitterText = local.text(viewLanguage, 'submitter');
    let providerText = local.text(viewLanguage, 'provider');
    let expectedText = local.text(viewLanguage, 'expected');
    // buttons
    let inspectText = local.text(viewLanguage, 'inspect');
    let deleteText = local.text(viewLanguage, 'delete');
    let closeText = local.text(viewLanguage, 'close');
    result.render('requests', {
        language_label: languageText,
        summary_label: summaryText,
        status_label: statusText,
        submitter_label: submitterText,
        provider_label: providerText,
        expected_label: expectedText,
        inspect_label: inspectText,
        delete_label: deleteText,
        refs: ref,
        languages: language,
        topics: topic,
        descriptions: description,
        statuses: status,
        close: closeText
    });
}

// inspection: ref, lang, summary, details, status, submitter, provider, expected
// selection: summary, status, submitter, provider, expected
function requestSelect(viewLanguage, account_name, permit, result) {
    pino.info('request select');
    let fetchRequestArgs = {
        data: {
            language: viewLanguage,
            account_name: account_name,
            permit: permit
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('fetch request args: %s', JSON.stringify(fetchRequestArgs));
    service.get(config.proTender + '/request', fetchRequestArgs, function(fetchResult) {
        pino.info('request fetch result count: %s', fetchResult.count);
        if (fetchResult.count === 0) {
            return;
        }
        pino.info('request fetch result data: %s', JSON.stringify(fetchResult.data));
        var ref = {};
        var language = {};
        var topic = {};
        var description = {};
        var status = {};
        for (var requestIndex = 0; requestIndex < fetchResult.count; requestIndex++) {
            pino.info('request %d: ref=%s, name=%s', requestIndex,
                fetchResult.data[requestIndex].ref,
                fetchResult.data[requestIndex].name);
            ref[requestIndex] = fetchResult.data[requestIndex].ref;
            language[requestIndex] = fetchResult.data[requestIndex].language;
            topic[requestIndex] = fetchResult.data[requestIndex].topic;
            description[requestIndex] = fetchResult.data[requestIndex].description;
            status[requestIndex] = fetchResult.data[requestIndex].status;
        }
        requestSelector(viewLanguage, result, fetchResult.count, 
            ref, language, topic, description, status);
    });
}

// inspection: ref, lang, summary, details, status, submitter, provider, expected
// selection: summary, status, submitter, provider, expected
function requestInspector(viewLanguage, result, ref, requestLanguage, topic, description, request, notes, submitterName, ownerName) {
    pino.info('request inspector: vlang = %s, ref = %s, topic = %s',
        viewLanguage, ref, topic);
    // first generate common text
    let sectionText = local.text(viewLanguage, 'request-section');
    let languageText = local.text(viewLanguage, 'language');
    let summaryText = local.text(viewLanguage, 'summary');
    let detailsText = local.text(viewLanguage, 'details');
    let statusText = local.text(viewLanguage, 'status');
    let submitterText = local.text(viewLanguage, 'submitter');
    let providerText = local.text(viewLanguage, 'provider');
    let expectedText = local.text(viewLanguage, 'expected');
    // button labels
    let saveText = local.text(viewLanguage, 'save');
    let closeText = local.text(viewLanguage, 'close');
    // render inspector
    result.render('request', {
        request_section: sectionText,
        ref: ref,
        language: languageText,
        language_value: requestLanguage,
        topic: topicText,
        topic_value: topic,
        description: descriptionText,
        description_value: description,
        status: statusText,
        status_value: status,
        notes: notesText,
        notes_value: notes,
        submitter_name: submitterNameText,
        submitter_name_value: submitterName,
        owner_name: ownerNameText,
        owner_name_value: ownerName,
        save: saveText,
        close: closeText
    });
}

// inspection: ref, lang, summary, details, status, submitter, provider, expected
// selection: summary, status, submitter, provider, expected
function requestInspect(language, account_name, permit, ref, result) {
    pino.info('request inspect ref=%s', ref);
    let requestFetchArgs = {
        data: {
            account_name: account_name,
            permit: permit,
            ref: ref
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for request fetch: %s', JSON.stringify(requestFetchArgs));
    service.get(config.proTender + '/request', requestFetchArgs, function(data) {
        pino.info('request fetch result: %s', data);
        requestInspector(language, result, data.data[0].ref,
            data.data[0].language, data.data[0].topic, data.data[0].description,
            data.data[0].status, data.data[0].notes, 
            data.data[0].submitter_name, data.data[0].owner_name
        );
    });
}

function requestPost(requestArgs) {
    service.post(config.proTender + '/request', requestArgs, function(data) {
        pino.info('request post returned: %s', JSON.stringify(data));
    });
}

function requestConfirm(language, requestRef, result) {
    pino.info('request delete confirm ref=%s', requestRef);
    let confirmText = local.text(language, 'request-delete-confirm');
    let closeText = local.text(language, 'close');
    let deleteText = local.text(language, 'delete');
    result.render('confirm-delete', {
        confirm_text: confirmText,
        close_text: closeText,
        delete_text: deleteText,
        type: 'request',
        ref: requestRef
    });
}

function requestDelete(account_name, permit, requestRef) {
    let deleteArgs = {
        data: {
            account_name: account_name,
            permit: permit,
            ref: requestRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('delete request, data: %s', JSON.stringify(deleteArgs.data));
    service.post(config.proTender + '/request/delete', deleteArgs, function(data) {
        pino.info('request delete returns: %s', JSON.stringify(data));
    });
}

request.create = requestCreate;
request.select = requestSelect;
request.inspect = requestInspect;
request.post = requestPost;
request.confirm = requestConfirm;
request.delete = requestDelete;
module.exports = request;
