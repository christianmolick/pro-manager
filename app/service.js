// service.js
// get and post data to account and pro rest-inspired services

// external modules
let pino = require('pino')();
let clientSource = require('node-rest-client').Client;
// object for exported routines
var service = {};

// service property get and post, possibly refactor to module
function propertyGet(url, args, callback) {
    // note: use of new in this context is no longer favored
    var client = new clientSource();
    var responseData = {};
    client.get(url, args, function(data, response) {
        if (response.statusCode === 404) {
            callback();
        } else {
            responseData = data;
            callback(responseData);
        }
    });
}

function propertyPost(url, args, callback) {
    var client = new clientSource();
    client.post(url, args, function(data, response) { 
        pino.info('service post response: %s',
            JSON.stringify(data));
        callback(data);
    });
}

service.get = propertyGet;
service.post = propertyPost;
module.exports = service;
