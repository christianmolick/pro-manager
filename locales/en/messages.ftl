# messages for base page
title = Twice Checked Group
about = About
contact = Contact
language-select = Language
account-section-label = Account
account-name-label = Name
created-label = Created
viewed-label = Viewed
modified-label = Modified
new-persona-label = New Persona
rename-persona-label = Rename Persona
name-label = Name
new-name-label = New Name
contact-label = Contact
new-contact-label = New Contact
address-label = Address
new-address-label = New Address
pronoun-label = Pronoun
new-pronoun-label = New Pronoun
qualifier-label = Qualifier
new-qualifier-label = New Qualifier
account-reference-label = Reference
account-sign-in = Sign in
account-sign-out = Sign out
account-inspect = Inspect Account
account-explain = Explain Accounts
account-reset = Reset Account
account-request = Request Account
account-delete = Delete Account
account-save = Save

# common panel messages
hide = Hide
close = Close
cancel = Cancel
select = Select
inspect = Inspect
personas = Personas
save = Save
update = Update
toggle = Toggle
verify = Verify
reset = Reset
delete = Delete
create = Create
explain = Explain
details = Details

# language panel and selection messages
language-options-label = Language Options
cancel-language-selection = Cancel
en = English
en-US = American English
en-GB = British English
es = Spanish
pt = Portuguese
pt-BR = Brazillian Portuguese
language-selected = Language selected

# account related explanations
account-explain = Explicar Contas
account-subject = Contas
account-body = Account records store names, contacts, addresses, pronouns, and notes for users, as well as their authentication credentials, roles, and status.
account-request-subject = Request Account
account-request-body = Accounts store authentication, contact, and name information with a named account.
account-name-invalid-subject = Invalid account name
account-name-invalid-body = Given account name is not valid
account-name-unavailable-subject = Unavailable account name
account-name-unavailable-body = Given account name is not available
account-created-subject = Account created
account-created-body = Requested account created and ready for sign in

# account related messages
sign-in-challenge = Respond to challenge to sign in
sign-in-success = Sign in successful
sign-in-failure = Sign in failed
sign-in-failed = Sign in failed
sign-out-successful =  Sign out successful
requesting-account = Complete and submit form to request account
account-created = Account created, sign in to use
account-updated = Account updated

# sign in, out messages
sign-section = Account sign in
explain-sign-in = Explain sign in
challenge-label = Challenge
response-label = Response
response-submit = Respond
response-cancel = Cancel

# account request and inspect field labels
request-account = Request Account
request-complete = Complete request
request-another = Request additional credentials
full-name-label = Full name
email-label = E-mail
phone-label = Phone
profile-label = Profile
permits-label = Permits
status-label = Status
roles-label = Roles
challenge-label = Challenge
response-label = Response
confirm-response-label = Confirm response
request-credential = Request credential
add-challenge = Add challenge
request-explain = Explain request

attributes-label = Attributes
value-label = Value
new-value-label = New Value
add = Add
new-attribute = New Attribute
credentials-label = Credentials
credential-section = Credentials
email-code = E-mail code

# persona related
select-persona = Select Persona
new-persona = New Persona
rename-persona = Rename Persona

# person actions, inspector fields, messages
person-section = Person
person-select = Select
person-create = Create
person-inspect = Inspect
person-delete-confirm = Confirm delete person
person-explain = Explain Persons
language-label = Language
notes-label = Notes
person-save = Save
person-subject = Persons
person-body = Person records store contact information and preferences for individuals and groups.

# person inspector
name = Name
full-name = Full Name
language = Language 
status = Status
profile = Profile
preferences = Preferences
provides = Provides

# place
place-section = Place
place-create = Create
place-inspect = Inspect
place-select = Select
place-explain = Explain Places
place-save = Save
place-delete-confirm = Confirm delete place
place-subject = Places
place-body = Place records store features of specific locations and general areas.

# place inspector
type = Type
new-type = New Type
description = Description
notes = Notes
container = Container

# item and item inspector
item-section = Item
item-create = Create
item-select = Select
item-inspect = Inspect
item-explain = Explain Items
item-save = Save
item-delete-confirm = Confirm delete item
item-subject = Items
item-body = Item records track specific materials, components, assemblies, and products

# issue
issue-section = Issue
issue-create = Create
issue-select = Select
issue-explain = Explain
issue-save = Save
issue-delete-confirm = Confirm delete issue
issue-subject = Issues
issue-body = Issue records track possible problems and optimizations
topic = Topic
submitter-name = Submitter
owner-name = Owner

# request
request-section = Request
request-create = Create
request-select = Select
request-explain = Explain

# process
process-section = Process
process-request = Request
process-create = Create
process-inspect = Inspect
process-select = Select
process-pay = Pay
process-review = Review
process-explain = Explain Process
requests-label = Requests
issues-label = Issues
services-label = Services
locations-label = Locations
providers-label = Provider
schedules-label = Schedules
request-label = Request
process-label = Process
server-label = Server
schedule-label = Schedule
process-save = Save

# process/event explanations
process-subject = Services
process-body = Services are requested by clients and bid on by providers.

# process inspector
brief = Brief
summary = Summary
client = Client
provider = Provider
start = Start
duration = Duration
fee = Fee

# event
event-section = Event
event-create = Create
event-select = Select
event-explain = Explain Events
event-subject = Events
event-body = Event records store details about service requests.
location = Location
person = Person
place = Place
event-delete-confirm = Confirm delete event
created = Created
viewed = Viewed
modified = Modified
proposed = Proposed
confirmed = Confirmed
expected = Expected
arrived = Arrived
started = Started
completed = Completed
billed = Billed
paid = Paid
reviewed = Reviewed

# result
result-section = Result
result-create = Create
result-select = Select
result-explain = Explain

# charge
charge-section = Charge
charge-create = Create
charge-select = Select
charge-explain = Explain

# payment
payment-section = Payment
payment-create = Create
payment-select = Select
payment-explain = Explain
