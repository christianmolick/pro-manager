# messages for base page
title = Grupo Verificado Dos Veces
about = Acerca de
contact = Contacto
language-select = Idiomas
account-section-label = Cuenta
account-name-label = Nombre
created-label = Creación
viewed-label = Visualización
modified-label = Modificación
new-persona-label = New Persona
rename-persona-label = Rename Persona
name-label = Nombre
new-name-label = Nuevo Nombre
contact-label = Contacto
new-contact-label = Nuevo Contacto
address-label = Dirección
new-address-label = Nuevo Dirección
pronoun-label = Pronombre
new-pronoun-label = Nuevo Pronombre
qualifier-label = Calificador
new-qualifier-label = Nuevo Calificador
account-reference-label = Referencia
account-sign-in = Inicia sesión
account-sign-out = Cerrar sesión
account-inspect = Cuenta Inspect
account-explain = Explicar Cuenta
account-reset = Restablecer Cuenta
account-request = Crear cuenta
account-delete = Borrar Account
account-save = Salvar

# common panel messages
hide = Esconder
close = Fechar
cancel = Cancelar
select = Seleccione
inspect = Inspecionar
personas = Personas
save = Salvar
update = Actualizar
toggle = Toggle
verify = Verificar
reset = Redefinir
delete = Excluir
create = Criar
explain = Explicar
details = Detalhes

# language panel and selection messages
language-options-label = Opcoes de idioma
cancel-language-selection = Cancelar
en = Ingles
en-US = Ingles Americano
en-GB = Ingles Britanico
es = Espanol
pt = Portugues
pt-BR = Brazillian Portuguese
language-selected = Idioma selecionado

# account related explanations
account-explain = Explicar Relatos
account-subject = Cuentas
account-body = Los registros de cuentas almacenan nombres, contactos, direcciones, pronombres y notas para los usuarios, así como sus credenciales de autenticación, funciones y estado.
account-request-subject = Solicitar cuenta
account-request-body = Accounts store authentication, contact, and name information with a named account.
account-name-invalid-subject = Invalid account name
account-name-invalid-body = Given account name is not valid
account-name-unavailable-subject = Unavailable account name
account-name-unavailable-body = Given account name is not available
account-created-subject = Cuenta creada, inicia sesión para usar
account-created-body = Requested account created and ready for sign in

# account related messages
sign-in-challenge = Responder al desafío para iniciar sesión
sign-in-success = Iniciar sesión correctamente
sign-in-failure = Fallo al iniciar sesion
sign-in-failed = Fallo al iniciar sesion
sign-out-successful =  Cerrar sesión correctamente
requesting-account = Complete y envíe el formulario para solicitar una cuenta
account-created = Cuenta creada, inicie sesión para usar
account-updated = Cuenta actualizada

# sign in, out messages
sign-section = Iniciar sesión en la cuenta
explain-sign-in = Explicar
challenge-label = Desafio
response-label = Respuesta
response-submit = Responder
response-cancel = Cancelar

# account request and inspect field labels
request-account = Solicitar cuenta
request-complete = Solicitud completa
request-another = Solicitar credenciales adicionales
full-name-label = Nombre completo
email-label = Correo electrónico
phone-label = Teléfono
profile-label = Perfil
permits-label = Permisos
status-label = Estado
roles-label = Roles
challenge-label = Desafío
response-label = Respuesta
confirm-response-label = Confirmar respuesta
request-credential = Solicitar credencial
add-challenge = Agregar desafío
request-explain = Explicar

attributes-label = Atributos
value-label = Valor
new-value-label = Nuevo Valor
add = Agregar
new-attribute = Nuevo Atributo
credentials-label = Cartas Credenciales
credential-section = Cartas Credenciales
email-code = E-mail code

# persona related
select-persona = Select Persona
new-persona = New Persona
rename-persona = Rename Persona

# person actions, inspector fields, messages
person-section = Persona
person-select = Seleccione
person-create = Crear
person-inspect = Inspeccionar
person-delete-confirm = Confirmar eliminar persona
person-explain = Explicar Personas
language-label = Idiomas
notes-label = Notas
person-save = Salvar
person-subject = Personas
person-body = Los registros de personas almacenan información de contacto y preferencias para individuos y grupos.

# person inspector
name = Nombre
full-name = Nom nom nombre
language = Idiomas
status = Estado
profile = Perfil
preferences = Preferencias
provides = Proporciona

# place
place-section = Colocar
place-create = Criar
place-inspect = Inspecionar
place-select = Selecionar
place-explain = Explicar Lugares
place-save = Salvar
place-delete-confirm = Confirmar eliminar sitio
place-subject = Lugares
place-body = Coloque las características de la tienda de registros de ubicaciones específicas y áreas generales.

# place inspector
type = Tipo
new-type = Nuevo Tipo
description = Descripción
notes = Notas
container = Envase

# item and item inspector
item-section = Articulo
item-create = Crear
item-select = Seleccione
item-inspect = Inspeccionar
item-explain = Explicar
item-save = Salvar
item-delete-confirm = Confirmar eliminar articulo
item-subject = Artículos
item-body = Item records track materials, components, assemblies, and products.

# issue
issue-section = Problema
issue-create = Crear
issue-select = Seleccione
issue-explain = Explicar
issue-save = Salvar
issue-delete-confirm = Confirmar eliminar problema
issue-subject = Issues
issue-body = Issue records track problems and possible optimizations.
topic = tema
submitter-name = Remitente
owner-name = Propietario

# request
request-section = Solicitud
request-create = Crear
request-select = Seleccione
request-explain = Explicar

# process
process-section = Proceso
process-request = Request
process-create = Create
process-inspect = Inspect
process-select = Select
process-pay = Pay
process-review = Review
process-explain = Explain Process
requests-label = Solicitud
issues-label = Cuestiones
services-label = Servicios
locations-label = Locations
providers-label = Provider
schedules-label = Schedules
request-label = Request
process-label = Process
server-label = Server
schedule-label = Schedule
process-save = Save

# process/event explanations
process-subject = Services
process-body = Services are requested by clients and bid on by providers.

# process inspector
brief = Brief
summary = Summary
client = Client
provider = Provider
start = Start
duration = Duration
fee = Fee

# event
event-section = Evento
event-create = Criar
event-select = Selecionar
event-explain = Explicar Eventos
event-subject = Eventos
event-body = Los registros de eventos almacenan detalles sobre las solicitudes de servicio.
location = Sitio
person = Persona
place = Lugar
event-delete-confirm = Confirmar eliminar evento
created = Criado
viewed = Visualizado
modified = Modificado
proposed = Proposto
confirmed = Confirmado
expected = Esperado
arrived = Chegado
started = Iniciado
completed = Concluido
billed = Faturado
paid = Pago
reviewed = Revisadas

# result
result-section = Resultado
result-create = Criar
result-select = Selecionar
result-explain = Explicar

# charge
charge-section = Cargar
charge-create = Criar
charge-select = Selecionar
charge-explain = Explicar

# payment
payment-section = Pago
payment-create = Criar
payment-select = Selecionar
payment-explain = Explicar
