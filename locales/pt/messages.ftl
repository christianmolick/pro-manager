# messages for base page
title = Grupo Verificado Duas Vezes
about = Sobre
contact = Contato
language-select = Idiomas
account-section-label = Conta
account-name-label = Nombre
created-label = Criação
viewed-label = visualização
modified-label = Modificação
new-persona-label = New Persona
rename-persona-label = Rename Persona
name-label = Name
new-name-label = New Name
contact-label = Contact
new-contact-label = New Contact
address-label = Address
new-address-label = New Address
pronoun-label = Pronoun
new-pronoun-label = New Pronoun
qualifier-label = Qualifier
new-qualifier-label = New Qualifier
account-reference-label = Reference
account-sign-in = Afirmar Conta
account-sign-out = Cerrar sesión
account-inspect = Conta inspect
account-explain = Explicar Conta
account-reset = Reset Account
account-request = Crear conta
account-delete = Delete Account
account-save = Salve

# common panel messages
hide = Ocultar
close = Fechar
cancel = Cancelar
select = Selecionar
inspect = Inspecionar
personas = Personas
save = Salve
update = Atualizar
toggle = Toggle
verify = Verificar
reset = Rearme
delete = Excluir
create = Crear
explain = Explicar
details = Detalhes

# language panel and selection messages
language-options-label = Opções de Idioma
cancel-language-selection = Cancelar
en = Ingles
en-US = Ingles Americano
en-GB = Inglas Britanico
es = Espanhol
pt = Portugues
pt-BR = Brazillan Portuguese
language-selected = Idioma selecionado

# account related explanations
account-explain = Explicar contas
account-subject = Contas
account-body = Os registros de conta armazenam nomes, contatos, endereços, pronomes e notas para usuários, bem como suas credenciais de autenticação, funções e status.
account-request-subject = Request Account
account-request-body = Accounts store authentication, contact, and name information with a named account.
account-name-invalid-subject = Invalid account name
account-name-invalid-body = Given account name is not valid
account-name-unavailable-subject = Unavailable account name
account-name-unavailable-body = Given account name is not available
account-created-subject = Conta criada
account-created-body = Requested account created and ready for sign in

# account related messages
sign-in-challenge = Responda ao desafio para fazer login
sign-in-success = Login bem sucedido
sign-in-failure = A entrada falhou
sign-in-failed = A entrada falhou
sign-out-successful =  Sair com sucesso
requesting-account = Preencher e enviar formulário para solicitar conta
account-created = Conta criada, faça login para usar
account-updated = Conta atualizada

# sign in, out messages
sign-section = Login da conta
explain-sign-in = Explicar
challenge-label = Desafio
response-label = Respuesta
response-submit = Responder
response-cancel = Cancelar

# account request and inspect field labels
request-account = Solicitar conta
request-complete = Solicitação completa
request-another = Solicitar credenciais adicionais
full-name-label = Nome completo
email-label = O email
phone-label = Telefone
profile-label = perfil
permits-label = Permitem
status-label = Status
roles-label = Funções
challenge-label = Desafio
response-label = Resposta
confirm-response-label = Confirmar resposta
request-credential = Solicitar credencial
add-challenge = Adicionar desafio
request-explain = Explicar o pedido

attributes-label = Atributos
value-label = Valor
new-value-label = Novo valor
add = Adicionar
new-attribute = Novo atributo
credentials-label = Credenciais
credential-section = Credenciais
email-code = E-mail code

# persona related
select-persona = Select Persona
new-persona = New Persona
rename-persona = Rename Persona

# person actions, inspector fields, messages
person-section = Pessoa
person-select = Selecionar
person-create = Crio
person-inspect = Inspeccionar
person-delete-confirm = Confirmar exclusão pessoa
person-explain = Explicar Pessoas
language-label = Língua
notes-label = Notas
person-save = Salve
person-subject = Pessoas
person-body = Os registros pessoais armazenam informações de contato e preferências de indivíduos e grupos.

# person inspector
name = Nombre
full-name = Nome completo
language = Língua
status = Status
profile = Perfil
preferences = Preferências
provides = Fornece

# place
place-section = Local
place-create = Crio
place-inspect = Inspeccionar
place-select = Selecionar
place-explain = Explicar Lugares
place-save = Salve
place-delete-confirm = Confirmar exclusão local
place-subject = Locais
place-body = Os registros de lugares armazenam recursos de locais específicos e áreas gerais.

# place inspector
type = Tipo
new-type = New Type
description = Descrição
notes = Notas
container = Recipiente

# item and item inspector
item-section = Item
item-create = Crio
item-select = Selecionar
item-inspect = Inspeccionar
item-explain = Explicar
item-save = Salve
item-delete-confirm = Confirmar exclusão item
item-subject = Items
item-body = Item records track materials, components, assemblies, and products.

# issue
issue-section = Questão
issue-create = Crio
issue-select = Selecionar
issue-explain = Explicar
issue-save = Salve
issue-delete-confirm = Confirmar exclusão questão
issue-subject = Questão
issue-body = Issues track problems and possible improvements
topic = Tema
submitter-name = Apresentador
owner-name = Proprietário

# request
request-section = Solicitação
request-create = Crio
request-select = Selecionar
request-explain = Explicar

# process
process-section = Processo
process-request = Request
process-create = Crio
process-inspect = Inspect
process-select = Selecionar
process-pay = Pay
process-review = Review
process-explain = Explicar
requests-label = Requests
issues-label = Issues
services-label = Services
locations-label = Locations
providers-label = Provider
schedules-label = Schedules
request-label = Request
process-label = Process
server-label = Server
schedule-label = Schedule
process-save = Salve

# process/event explanations
process-subject = Services
process-body = Services are requested by clients and bid on by providers.

# process inspector
brief = Brief
summary = Summary
client = Client
provider = Provider
start = Start
duration = Duration
fee = Fee

# event
event-section = Evento
event-create = Crio
event-select = Selecionar
event-explain = Explicar Eventos
event-subject = Eventos
event-body = Os registros de eventos armazenam detalhes sobre solicitações de serviço.
location = Location
person = Pessoa
place = Lugar
event-delete-confirm = Confirmar exclusão evento
created = Criado
viewed = Viesualizado
modified = Modificado
proposed = Proposto
confirmed = Confirmado
expected = Esperado
arrived = Chegado
started = Iniciado
completed = Concluído
billed = Faturado
paid = Pago
reviewed = Revisado

# result
result-section = Resultado
result-create = Crio
result-select = Selecionar
result-explain = Explicar

# charge
charge-section = Custo
charge-create = Crio
charge-select = Selecionar
charge-explain = Explicar

# payment
payment-section = Forma de pagamento
payment-create = Crio
payment-select = Selecionar
payment-explain = Explicar
