sudo -u postgres dropdb browser_session
sudo -u postgres createdb -O tender --encoding=utf8 browser_session "simple browser session store"
sudo -u postgres psql -d browser_session -e -f node_modules/connect-pg-simple/table.sql
sudo -u postgres psql -d browser_session -e --command="GRANT ALL PRIVILEGES ON DATABASE browser_session TO tender;"
sudo -u postgres psql -d browser_session -e --command="GRANT ALL PRIVILEGES ON TABLE session TO tender;"
